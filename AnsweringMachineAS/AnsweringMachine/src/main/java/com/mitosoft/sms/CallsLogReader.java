package com.mitosoft.sms;

import android.app.Service;
import android.database.Cursor;
/**
 * @deprecated Not used any more. Call State Listener class provides us with phone number instantly!
 * 
 * This calss reads the last missed call from the OS logs. 
 * 
 * */
public class CallsLogReader {

	public CallsLogReader() {}
	
	public String getLastMissedCallNumber(Service caller) {
		String result = null;
		
		// Querying for a cursor is like querying for any SQL-Database
		Cursor c = caller.getContentResolver().query(
				android.provider.CallLog.Calls.CONTENT_URI,
				null, null, null, 
				android.provider.CallLog.Calls.DATE + " DESC");		
		
		
		// Retrieve the column-indexes of phoneNumber, date and call type
		int numberColumn = c.getColumnIndex(
				android.provider.CallLog.Calls.NUMBER);
		int dateColumn = c.getColumnIndex(
				android.provider.CallLog.Calls.DATE);
		// type can be: Incoming, Outgoing or Missed
		int typeColumn = c.getColumnIndex(
				android.provider.CallLog.Calls.TYPE);
		
		// Loop through all entries the cursor provides to us.		
		if(c.moveToFirst()){
			do{
				result = c.getString(numberColumn);
				int callDate = c.getInt(dateColumn);
				int callType = c.getInt(typeColumn);
				
				if (callType == android.provider.CallLog.Calls.MISSED_TYPE) {										
					break;
				}
				
			}while(c.moveToNext());						
		}
		
		return result;
	}
}
