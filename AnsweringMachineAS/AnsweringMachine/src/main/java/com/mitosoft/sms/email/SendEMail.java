package com.mitosoft.sms.email;

import com.mitosoft.sms.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SendEMail extends Activity {
	
	private Button m_btnSend = null;
	private EditText m_txtEMail = null;
	private String m_strEMailBody = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sendsuggestion);
		
		m_txtEMail = (EditText) findViewById(R.id.editTextEMail);
        m_btnSend = (Button) findViewById(R.id.buttonSend);
        
        m_btnSend.setOnClickListener(new View.OnClickListener() {						
			public void onClick(View v) {				
				m_strEMailBody = m_txtEMail.getText().toString();
				if (m_strEMailBody.length() > 0) {
					sendEmail2();
				} else {
					Toast.makeText(SendEMail.this, "Enter e mail text.", Toast.LENGTH_LONG).show();
				}
			}
		});
	}
		   
	protected void sendEmail() {	          
	    final Intent emailIntent = new Intent( android.content.Intent.ACTION_SENDTO );
	      emailIntent.setType("plain/text");
	      emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
	              new String[] { "divukman@gmail.com" });
	      emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
	              "Email Subject");
	      emailIntent.putExtra(android.content.Intent.EXTRA_TEXT,
	              "Email Body");
	      startActivity(Intent.createChooser(
	              emailIntent, "Send mail..."));
	}
	
	protected void sendEmail2() {
		// Setup the recipient in a String array
		String[] mailto = { "divukman@gmail.com" };		
		// Create a new Intent to send messages
		Intent sendIntent = new Intent(Intent.ACTION_SEND);
		// Add attributes to the intent
		sendIntent.putExtra(Intent.EXTRA_EMAIL, mailto);		
		sendIntent.putExtra(Intent.EXTRA_TEXT, m_strEMailBody);
		sendIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
        "Android SMS Answering Machine - User Feedback");
		//sendIntent.setType("message/rfc822");
		sendIntent.setType("text/plain");
		startActivity(Intent.createChooser(sendIntent, "Please pick your preferred email application"));
	}
}
