package com.mitosoft.sms;

import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class SettingsActivity extends Activity {

	private Button m_btnApply = null;
	private Spinner m_spnTimePeriod = null;
	private Spinner m_spnRingerMode = null;
	private EditText m_txtLandLinePrefixes = null;
	private CheckBox m_chkBoxEveryone = null;
	private CheckBox m_chkBoxContacts = null;	
	
	private CheckBox m_chkBoxNotContacts = null;
	
	private CheckBox m_chkBoxForwardEMailMC = null;
	private CheckBox m_chkBoxForwardEmailTextM = null;
	private EditText m_txtEditEMail = null;
	
	private EditText m_txtEditSMTPHost = null;
	private EditText m_txtEditSMTPPort = null;
	private EditText m_txtEditSMTPUsername = null;
	private EditText m_txtEditSMTPPassword = null;
	
	private CheckBox m_chkBoxPriority = null;
	
	public final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
	          "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
	          "\\@" +
	          "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
	          "(" +
	          "\\." +
	          "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
	          ")+"
	      );

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);
		
		m_btnApply = (Button)findViewById(R.id.buttonSettingsApply);
		m_btnApply.setOnClickListener(new View.OnClickListener() {						
			public void onClick(View v) {
				savePreferences();
				finishSettingsActivity();
			}
		});
		
		m_spnTimePeriod = (Spinner) findViewById(R.id.spinnerSettingsTimePeriod);
		m_spnRingerMode = (Spinner) findViewById(R.id.spnRingerMode);
		
		m_txtLandLinePrefixes = (EditText) findViewById(R.id.editTextSettingsExclude);
		m_chkBoxEveryone = (CheckBox) findViewById(R.id.checkBoxSettingsEveryone);
		m_chkBoxContacts = (CheckBox) findViewById(R.id.checkBoxContacts);		
		
		m_chkBoxNotContacts = (CheckBox) findViewById(R.id.checkBoxNotInContacts);
		
		m_chkBoxForwardEMailMC = (CheckBox) findViewById(R.id.checkBoxForwardToEMailMC);
		m_chkBoxForwardEmailTextM = (CheckBox) findViewById(R.id.checkBoxForwardToEMAILTextM);
		m_txtEditEMail = (EditText) findViewById(R.id.editTextEMail);
		
		m_txtEditSMTPHost = (EditText) findViewById(R.id.editTextSMTPHost);
		m_txtEditSMTPPort = (EditText) findViewById(R.id.editTextSMTPPort);
		m_txtEditSMTPUsername = (EditText) findViewById(R.id.editTextSMTPUsername);
		m_txtEditSMTPPassword = (EditText) findViewById(R.id.editTextSMTPPassword);
		
		m_chkBoxPriority = (CheckBox) findViewById(R.id.checkBoxSettingsPriority);
		
		restorePreferences();
				
		m_chkBoxEveryone.setOnClickListener(new View.OnClickListener() {		
			public void onClick(View v) {
				boolean checked = m_chkBoxEveryone.isChecked();
				m_chkBoxContacts.setChecked(!checked);
				if (checked) {					
					m_chkBoxNotContacts.setChecked(!checked);	
				} 
			}							
		});
		
		
		m_chkBoxContacts.setOnClickListener(new View.OnClickListener() {			
			public void onClick(View v) {
				boolean checked = m_chkBoxContacts.isChecked();
				m_chkBoxEveryone.setChecked(!checked);
				if (checked) {					
					m_chkBoxNotContacts.setChecked(!checked);
				}				
			}
		});
		
		m_chkBoxNotContacts.setOnClickListener (new View.OnClickListener() {			
			public void onClick(View v) {
				boolean checked = m_chkBoxNotContacts.isChecked();
				m_chkBoxEveryone.setChecked(!checked);
				if (checked) {										
					m_chkBoxContacts.setChecked(!checked);
				}				
			}
		});			
	}
	
	private void finishSettingsActivity() {
		Intent resultIntent = new Intent();
		//resultIntent.putExtra(PUBLIC_STATIC_STRING_IDENTIFIER, tabIndexValue);
		setResult(Activity.RESULT_OK, resultIntent);
		finish();
	}
	
	private void savePreferences() {
		// We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences settings = getSharedPreferences(mainActivity.PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        
        String strSpinnerVal = (String)m_spnTimePeriod.getSelectedItem();
        Integer spinnerValue = Integer.valueOf(strSpinnerVal); 
        int intTimePeriod = spinnerValue.intValue();
        
        String textBoxValue = m_txtLandLinePrefixes.getText().toString();
        String [] strArrLandLinePrefixes = textBoxValue.split(",");
        
        boolean boolReplyToEveryone = m_chkBoxEveryone.isChecked();
        boolean boolReplyToContactsOnly = m_chkBoxContacts.isChecked();       
        boolean boolReplyToNotcontacts = m_chkBoxNotContacts.isChecked();
        
        boolean boolHighPriority = m_chkBoxPriority.isChecked();
        
        String strRingerMode = (String)m_spnRingerMode.getSelectedItem();
        
        boolean boolForwardToEMailMissedCalls = m_chkBoxForwardEMailMC.isChecked();
        boolean boolForwardToEMailTextM = m_chkBoxForwardEmailTextM.isChecked();
        String strEMailToForwardTo = m_txtEditEMail.getText().toString().trim();
        
        
        if (!isValidEMailAddress(strEMailToForwardTo)) {
        	boolForwardToEMailMissedCalls = false;
        	boolForwardToEMailTextM = false;
        	Toast.makeText(this, "E mail is not valid!", Toast.LENGTH_SHORT).show();        	
        } else {
        	editor.putString(mainActivity.PREFS_SETTINGS_FORWARD_TO_THIS_EMAIL, strEMailToForwardTo);	
        }
        
        String strSMTPHost = m_txtEditSMTPHost.getText().toString().trim();
        String strSMTPPort = m_txtEditSMTPPort.getText().toString().trim();
        String strSMTPUsername = m_txtEditSMTPUsername.getText().toString().trim();
        String strSMTPPAssword = m_txtEditSMTPPassword.getText().toString().trim();
        
        if ( (strSMTPHost.length() < 1) || (strSMTPPort.length() < 1) ) {
        	boolForwardToEMailMissedCalls = false;
        	boolForwardToEMailTextM = false;
        	Toast.makeText(this, "Host/port invalid!", Toast.LENGTH_SHORT).show();  
        }
        
        //todo : checking, button to send test e mail
        editor.putString(mainActivity.PREFS_SETTINGS_SMTP_HOST, strSMTPHost);
        editor.putString(mainActivity.PREFS_SETTINGS_SMTP_PORT, strSMTPPort);
        editor.putString(mainActivity.PREFS_SETTINGS_SMTP_USERNAME, strSMTPUsername);
        editor.putString(mainActivity.PREFS_SETTINGS_SMTP_PASSWORD, strSMTPPAssword);
            	
        editor.putBoolean(mainActivity.PREFS_SETTINGS_FORWARD_TO_EMAIL_MC, boolForwardToEMailMissedCalls);
        editor.putBoolean(mainActivity.PREFS_SETTINGS_FORWARD_TO_EMAIL_TEXT_M, boolForwardToEMailTextM);
        editor.putBoolean(mainActivity.PREFS_SETTINGS_PRIORITY, boolHighPriority);
        
        //todo get selected contacts, if, and then store to prefs if possible, if not, use sql db u dummy
        editor.putInt(mainActivity.PREFS_SETTINGS_TIME_PERIOD, intTimePeriod);
        editor.putString(mainActivity.PREFS_SETTINGS_LAND_LINES_PREFIXES, textBoxValue);
        editor.putBoolean(mainActivity.PREFS_SETTINGS_REPLY_TO_EVERYONE, boolReplyToEveryone);
        editor.putBoolean(mainActivity.PREFS_SETTINGS_REPLY_TO_CONTACTS, boolReplyToContactsOnly);        
        editor.putBoolean(mainActivity.PREFS_SETTINGS_REPLY_TO_NOT_CONTACTS, boolReplyToNotcontacts);
                
        int ringerMode = getRingerTypeFromString(strRingerMode);
        editor.putInt(mainActivity.PREFS_SETTINGS_RINGER_MODE, ringerMode);
        
        // Commit the edits!
        editor.commit();
	}
	
	private void restorePreferences() {		
		 // Restore preferences
        SharedPreferences settings = getSharedPreferences(mainActivity.PREFS_NAME, 0);
        if (settings != null) {
	        //m_strAnswerText = settings.getString(PASS_DATA_ANSWER_TEXT, "Answering machine: I will call you later.");
        	int spinnerVal = settings.getInt(mainActivity.PREFS_SETTINGS_TIME_PERIOD, 1);
        	m_spnTimePeriod.setSelection(spinnerVal - 1);
        	
        	String landLinePrefixes = settings.getString(mainActivity.PREFS_SETTINGS_LAND_LINES_PREFIXES, "+38521,");
        	m_txtLandLinePrefixes.setText(landLinePrefixes);
        	
        	boolean boolEveryone = settings.getBoolean(mainActivity.PREFS_SETTINGS_REPLY_TO_EVERYONE, true);
        	m_chkBoxEveryone.setChecked(boolEveryone);
        	
        	boolean boolHighPriority = settings.getBoolean(mainActivity.PREFS_SETTINGS_PRIORITY, false);
        	m_chkBoxPriority.setChecked(boolHighPriority);
        	
        	boolean boolContacts = settings.getBoolean(mainActivity.PREFS_SETTINGS_REPLY_TO_CONTACTS, false);
        	m_chkBoxContacts.setChecked(boolContacts);
        	
        	boolean boolNotContacts = settings.getBoolean(mainActivity.PREFS_SETTINGS_REPLY_TO_NOT_CONTACTS, false);
        	m_chkBoxNotContacts.setChecked(boolNotContacts);
        	
        	String strEMailToForwardTo = settings.getString(mainActivity.PREFS_SETTINGS_FORWARD_TO_THIS_EMAIL, "n/a");
        	boolean boolForwardToEMailMC = settings.getBoolean(mainActivity.PREFS_SETTINGS_FORWARD_TO_EMAIL_MC, false);
        	boolean boolForwardToEMailTextM = settings.getBoolean(mainActivity.PREFS_SETTINGS_FORWARD_TO_EMAIL_TEXT_M, false);
        	
        	m_txtEditEMail.setText(strEMailToForwardTo);
        	m_chkBoxForwardEMailMC.setChecked(boolForwardToEMailMC);
        	m_chkBoxForwardEmailTextM.setChecked(boolForwardToEMailTextM);  
        	        	
        	String strSMTPHost = settings.getString(mainActivity.PREFS_SETTINGS_SMTP_HOST, "smtp.gmail.com");
        	String strSMTPPort = settings.getString(mainActivity.PREFS_SETTINGS_SMTP_PORT, "465");
        	String strSMTUsername = settings.getString(mainActivity.PREFS_SETTINGS_SMTP_USERNAME, "n/a");
        	String strSMTPPassword = settings.getString(mainActivity.PREFS_SETTINGS_SMTP_PASSWORD, "n/a");
        	
        	m_txtEditSMTPHost.setText(strSMTPHost);
        	m_txtEditSMTPPort.setText(strSMTPPort);
        	m_txtEditSMTPUsername.setText(strSMTUsername);
        	m_txtEditSMTPPassword.setText(strSMTPPassword);
        	}
	                	        
	        int intRingerMode = settings.getInt(mainActivity.PREFS_SETTINGS_RINGER_MODE, AudioManager.RINGER_MODE_NORMAL);	        
	        int position = getRingerTypeSpinnerPositionFromInt(intRingerMode);	        	        	        
	    	m_spnRingerMode.setSelection(position);        
	}
	
	private int getRingerTypeFromString (String val) {
		int result = 0;
		
		String str_normal = getString(R.string.str_noraml);
        String str_silent = getString(R.string.str_silent);
        String str_vibrate = getString(R.string.str_vibrate);
        
        if (val.equalsIgnoreCase(str_normal)) {
        	result = AudioManager.RINGER_MODE_NORMAL;
        } else if (val.equalsIgnoreCase(str_silent)) {
        	result = AudioManager.RINGER_MODE_SILENT;
        } else if (val.equalsIgnoreCase(str_vibrate)) {
        	result = AudioManager.RINGER_MODE_VIBRATE;
        }
		
		return result;
	}
	
	private int getRingerTypeSpinnerPositionFromInt(int val) {
		int result = 0;
		
		if (val == AudioManager.RINGER_MODE_NORMAL) {
			result = 0;
		} else if (val == AudioManager.RINGER_MODE_SILENT) {
			result = 1;
		} else if (val == AudioManager.RINGER_MODE_VIBRATE) {
			result = 2;
		}
		
		return result;
	}
	
	private boolean isValidEMailAddress(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
}

}
