package com.mitosoft.sms;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.mitosoft.event.CommunicationEvent;
import com.mitosoft.event.Event;
import com.mitosoft.event.EventType;
import com.mitosoft.event.ICommEventListener;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;
 
public class SMSReceiver extends BroadcastReceiver
{	
	private List m_eventListeners = new ArrayList();
		
	public void addEventListener(ICommEventListener l) {
		m_eventListeners.add(l);
	}
	
	public void removeEventListener(ICommEventListener l) {
		m_eventListeners.remove(l);
	}
	
	private void fireEvent(CommunicationEvent e) {
		Iterator listeners = m_eventListeners.iterator();
		while (listeners.hasNext()) {
			( (ICommEventListener) listeners.next() ).onEvent(e);
		}
	}
	
	
    @Override
    public void onReceive(Context context, Intent intent) 
    {
        //---get the SMS message passed in---
        Bundle bundle = intent.getExtras();        
        SmsMessage[] msgs = null;
        String str = "";        
        if (bundle != null) {
            //---retrieve the SMS message received---
            Object[] pdus = (Object[]) bundle.get("pdus");
            msgs = new SmsMessage[pdus.length];            
            for (int i=0; i<msgs.length; i++){
                msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);                
                str += "SMS from " + msgs[i].getOriginatingAddress();                   
                str += " :";
                str += msgs[i].getMessageBody().toString();
                str += "\n";        
                Log.i(AnsweringMachineService.LOG_TAG, "Message received. " + str);
                //m_parent.replyToSMS(msgs[i].getOriginatingAddress());
                String address = msgs[i].getOriginatingAddress();
                CommunicationEvent event = new CommunicationEvent(this,EventType.SMS_RECEIVED, address, msgs[i].getMessageBody().toString());
                fireEvent(event);
            } 
            //---display the new SMS message---
            Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
        } else {
        	Log.e(AnsweringMachineService.LOG_TAG, "SMSReceiver, bundle is null.");
        }                         
    }
}