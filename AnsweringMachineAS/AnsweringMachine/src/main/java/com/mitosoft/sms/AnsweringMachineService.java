package com.mitosoft.sms;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import com.mitosoft.answeringmachine.alarms.AlarmReceiver;
import com.mitosoft.event.CommunicationEvent;
import com.mitosoft.event.Event;
import com.mitosoft.event.EventType;
import com.mitosoft.event.ICommEventListener;
import com.mitosoft.sms.R.drawable;
import com.mitosoft.sms.email.EMailHelper;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.ContactsContract.PhoneLookup;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

public class AnsweringMachineService extends Service implements ICommEventListener {

	public static final String LOG_TAG = "Answering Machine service:";
	private String m_strAnswer = null;
	private boolean m_boolAnserSMS = false;
	private boolean m_boolAnserMC = false;
	private boolean m_boolHigherPriority = false;
	private int m_intTimePeriod = 8; //hours
	private String m_strLandLinePrefixes = "+38521,";
	private boolean m_boolReplyToEveryone = true;
	private boolean m_boolReplyToContacts = false;
	private boolean m_boolReplyToNotContacts = false;
	private int m_intRingerTypeWhileActive = AudioManager.RINGER_MODE_NORMAL;
	private int m_intRingerTypeOld = AudioManager.RINGER_MODE_NORMAL;
	//todo selected contacts and later regexp 4 dummies support
	
	private String m_strEMailToForwardTo = null;
	private boolean m_boolForwardToEMailMC = false;
	private boolean m_boolForwardToEMailTextM = false;
	
	private boolean m_boolOnlyEMailForwarding = false;
	
	private String m_strHost = null;
	private String m_strPort = null;
	private String m_strUsername = null;
	private String m_strPassword = null;
	
	private CallStateListener m_callStateListner = null;
	private SMSReceiver m_smsReceiver = null;
	
	/*Hash map of previous callers/sms senders. Caller's/sender's number is stored in the hash map
	 * along with time the call/message was received. 
	 * 
	 * When answering call/sms:
	 * 	If caller/sender number is in the map, do not answer.
	 * 	If caller/sender number is not in the map, answer and add pair number/timeinmillis to map.
	 * 	Purge map on timer task scheduled to time period selected in advanced settings, default of 1h. 
	 * */
	
	private volatile HashMap<String, Long> m_hashMapCallersList = null;
	private TimerTask m_timerTaskClearHistory = null;
	private Timer m_timer = null;
	
	private List<String> m_lstLandLinePrefixes = null;
	
	@Override
	public IBinder onBind(Intent arg0) {		
		return null;
	}
	
	
	private void startServiceInForeground() {		
		Notification note = new Notification(R.drawable.ic_launcher_main, "Answering Machine Running", System.currentTimeMillis());
		Intent i = new Intent(this, mainActivity.class);		
		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		
		PendingIntent pi = PendingIntent.getActivity(this, 0,i, 0);
		note.setLatestEventInfo(this, "Answering Machine Service", "Active", pi);
		note.flags|= Notification.FLAG_NO_CLEAR;
		note.icon = drawable.ic_launcher_main;
		
		Log.i(AnsweringMachineService.LOG_TAG, "Starting service in foreground.");
		startForeground(1337, note);
	}
	
	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		super.onStart(intent, startId);
		
		readPreferencesFromIntentsBundle(intent);
		
		m_timerTaskClearHistory = new TimerTask() {
			@Override
			public void run() {				
				purgeCallHistory();
			}
		};
		
		long delay = m_intTimePeriod * 3600000;
		m_timer = new Timer();
		m_timer.schedule(m_timerTaskClearHistory, delay, delay);
		
		m_lstLandLinePrefixes = new ArrayList<String>();
		String [] arrPrefixes = m_strLandLinePrefixes.split(",");
		for (int i = 0; i < arrPrefixes.length; i++) {
			m_lstLandLinePrefixes.add(arrPrefixes[i]);
		}
		
		 //Toast.makeText(this,"Answering Machine Service started ..." + m_strAnswer + m_boolAnserMC + m_boolAnserSMS, Toast.LENGTH_LONG).show();
		 Toast.makeText(this,"Answering Machine Service running ...", Toast.LENGTH_SHORT).show();
		 
		 if (m_boolAnserMC) {
			 activateCallStateListener();
		 }
		 
		 if(m_boolAnserSMS) {
			 activateSMSListener();
		 }
		 
		 if (m_boolOnlyEMailForwarding) {
			 activateCallStateListener();
			 activateSMSListener();
		 }
		 
		 readAndSetCurrentRingerType();
		 setRingerMode(m_intRingerTypeWhileActive);
		 
		 startServiceInForeground();
	}
	
	  @Override
      public void onCreate() {
            super.onCreate();
            m_hashMapCallersList = new HashMap<String, Long>();
            //Toast.makeText(this,"Answering Machine Service created ...", Toast.LENGTH_SHORT).show();
      }
     
      @Override
      public void onDestroy() {
            super.onDestroy();
            
            cancelAutoShutDownSchedule();
            
            if (m_timer != null) {
            	Log.w(LOG_TAG, "onDestroy: m_timer already null");
	            m_timer.cancel();
	            m_timer = null; 
            }
            
            m_timerTaskClearHistory = null;
            
            if (m_hashMapCallersList != null) {
            	Log.w(LOG_TAG, "onDestroy: callers list already null");
	            m_hashMapCallersList.clear();
	            m_hashMapCallersList = null; 
            }
            
            if (m_lstLandLinePrefixes != null) {
            	Log.w(LOG_TAG, "onDestroy: prefixes already null");
	            m_lstLandLinePrefixes.clear();
	            m_lstLandLinePrefixes = null;
            }     
            
            if (m_boolAnserMC) {
            	deactivateCallStateListener(); 
            }
            if (m_boolAnserSMS) {
            	deactivateSMSListener();
            }
            
            setRingerMode(m_intRingerTypeOld);
            
            Toast.makeText(this, "Answering Machine Service stopped ...", Toast.LENGTH_SHORT).show();
      }
      
      private void activateCallStateListener() {     	  
    	  m_callStateListner = new CallStateListener();
    	  m_callStateListner.addEventListener(this);
          TelephonyManager telephony = (TelephonyManager)
          this.getSystemService(Context.TELEPHONY_SERVICE);
          telephony.listen(m_callStateListner,PhoneStateListener.LISTEN_CALL_STATE);
          Log.i(AnsweringMachineService.LOG_TAG, "Call state listener activated.");
     }
      
      private void activateSMSListener() {
     	 m_smsReceiver = new SMSReceiver();
     	 m_smsReceiver.addEventListener(this);
     	 
     	 IntentFilter receiverIntentFilter =  new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
     	 
     	 if (m_boolHigherPriority) {
     		 receiverIntentFilter.setPriority(9999);
     	 }     	 
     	 registerReceiver(m_smsReceiver, receiverIntentFilter);
     	
     	Log.i(AnsweringMachineService.LOG_TAG, "SMS listener activated.");
      }

      private void deactivateCallStateListener() {    	
    	  if (m_callStateListner != null) {
    		  m_callStateListner.removeEventListener(this);
	    	  TelephonyManager telephony = (TelephonyManager)
	          this.getSystemService(Context.TELEPHONY_SERVICE);
	          telephony.listen(m_callStateListner,PhoneStateListener.LISTEN_NONE);
	          m_callStateListner = null; 
	          Log.i(AnsweringMachineService.LOG_TAG, "Call state listener deactivated.");
          }
      }
      
      private void deactivateSMSListener() {
    	  if (m_smsReceiver != null) {
    		  m_smsReceiver.removeEventListener(this);
	    	  unregisterReceiver(m_smsReceiver);
	    	  m_smsReceiver = null; 
	    	  Log.i(AnsweringMachineService.LOG_TAG, "SMS listener deactivated.");
    	  }
      }
            
     
     public void replyWithSMSToPhoneNumber(String phoneNumber, CommunicationEvent commEvent) {
    	 String myPhoneNumberOnLine1 = getMyPhoneNumber();
		 if (myPhoneNumberOnLine1 != null && !myPhoneNumberOnLine1.equalsIgnoreCase(phoneNumber)) {
			 sendSMS(phoneNumber, m_strAnswer, commEvent); 
    	 } else if (myPhoneNumberOnLine1 == null) {
    		 Toast.makeText(this, "Your operator did not store your phone number on your sim card! \n Do not send SMS to your self to avoid any looping!", Toast.LENGTH_LONG);
			 sendSMS(phoneNumber, m_strAnswer, commEvent);
    	 }
     }
     
     private void sendSMS(String phoneNumber, String message, CommunicationEvent commEvent) {  
    	 Calendar c= Calendar.getInstance();
    	 if (!m_hashMapCallersList.containsKey(phoneNumber)) {
    		 
    		 boolean proceed = true;
    		 
    		 Iterator<String> iter = m_lstLandLinePrefixes.iterator();
    		 while (iter.hasNext()) {
    			 String prefix = iter.next();
    			 // temporary hack to fix bug Colton discovered. "" gets stored as a land line prefix
    			 // and below comparison always, in that case, passes and then it does not send sms
    			 //if (phoneNumber.startsWith(prefix)) {
    			 if ( (phoneNumber == null) || (!prefix.equalsIgnoreCase("") && phoneNumber.startsWith(prefix) ) ) {
    				 proceed = false;
    				 Log.i(LOG_TAG, "Call was from a land line prefix OR private number. Not sending SMS.");
    				 break;
    			 }
    		 }
    		 /*
    		 if (proceed && m_boolReplyToContacts) {
    			 if (!contactExists(this, phoneNumber)) {
    				 proceed = false;
    				 Log.i(LOG_TAG, "Not in contacts list ("+ phoneNumber +"). Not sending SMS.");
    			 }
    		 } else if (proceed && m_boolReplyToNotContacts) {
    			 if (contactExists(this, phoneNumber)) {    				 
    				 proceed = false;
    				 Log.i(LOG_TAG, "Contact is in the contacts list ("+ phoneNumber +"). Not sending SMS to contacts.");
    			 }
    		 }*/
    		 
    		 if (proceed) {    			 
    			 if (m_boolReplyToContacts) {
    				 if (!contactExists(this, phoneNumber)) {
        				 proceed = false;
        				 Log.i(LOG_TAG, "Not in contacts list ("+ phoneNumber +"). Not sending SMS.");
        			 } 
    			 } else if (m_boolReplyToNotContacts) {
    				 if (contactExists(this, phoneNumber)) {    				 
        				 proceed = false;
        				 Log.i(LOG_TAG, "Contact is in the contacts list ("+ phoneNumber +"). Not sending SMS to contacts.");
        			 } 
    			 } else if (m_boolReplyToEveryone){   
    				 if (m_boolOnlyEMailForwarding) {
    					 proceed = false;
    					 Log.i(LOG_TAG, "Reply to everyone is true, but only e mail forwarding is set. Not replying to ("+ phoneNumber +").");
    				 } else {
    					 Log.i(LOG_TAG, "Replying to everyone. Sending message to: ("+ phoneNumber +").");
    				 }
    			 } else if (m_boolOnlyEMailForwarding) {
    				 Log.i(LOG_TAG, " Not replying to anyone! I will forward to e mail though! ("+ phoneNumber +").");
    				 proceed = false;
    			 }
    		 }
    		 
    		 if (proceed) {    			 
		    	 m_hashMapCallersList.put(phoneNumber, c.getTimeInMillis());
		         PendingIntent pi = PendingIntent.getActivity(this, 0,
		             new Intent(this, AnsweringMachineService.class), 0);    
		         //old code throws exception if message exceeds 160 chars
		         
		         //SmsManager sms = SmsManager.getDefault();
		         //sms.sendTextMessage(phoneNumber, null, message, pi, null);
		         //new code		         
		         SmsManager sms = SmsManager.getDefault();
		         
		         //<hack>: message must not be null
		         if ( (message == null) || (message.trim().length() == 0)) {
		        	 message = "Answering Machine Automated Reply: I am not available!";
		        	 Log.e(LOG_TAG, "Answering Machine Service: Reply message is somehow NULL! Using default string!");
		         } 
		         //</hack>
		         
		         try {		        	 	
		        	 ArrayList<String> parts = sms.divideMessage(message);
		        	 ArrayList<PendingIntent> piLst = new ArrayList<PendingIntent>(); 
			         piLst.add(pi);
			         Log.i(LOG_TAG, "Sending SMS to " + phoneNumber + " Sms has: " + parts.size() + " parts.");
			         
			         if (isValidNumber(phoneNumber)) {
			        	 sms.sendMultipartTextMessage(phoneNumber, null, parts, piLst, null);
				         addMessageToOutbox(phoneNumber, message);	 
			         } else {
			        	 Log.w(LOG_TAG, "Not valid phone number (perhaps VOIP ID?). Not sending sms. " + phoneNumber);
			         }		         		         
		         } catch (RuntimeException ex) {
		        	 Log.e(LOG_TAG, "Error sending sms. Possible error in divide sms from android. Msg: " + ex.getLocalizedMessage());
		        	 ex.printStackTrace();		        	 
		         }
		         		         
	         } 
    		 
    		 if (proceed && (!m_boolOnlyEMailForwarding)) {
    			 if ( m_boolForwardToEMailMC && (commEvent.getType() == EventType.CALL_MISSED) ) {
    				 //send e mail
    				 sendEMail("SMS Answering Machine: missed call from " + phoneNumber, "Reply sent: " + message);
    			 } else if (m_boolForwardToEMailTextM && (commEvent.getType() == EventType.SMS_RECEIVED)) {
    				 //send e mail
    				 sendEMail("SMS Answering Machine: SMS received from " + phoneNumber, "Message: "+ commEvent.getMessage() +  " \n\nReply sent: " + message);
    			 }
    		 }	 
         } else {        	   
        	 Log.i(LOG_TAG, "Already answered to this phone number. " + phoneNumber + "). Not sending SMS.");
         }    
    	 
    	 if (m_boolOnlyEMailForwarding) {
			 if ( (commEvent.getType() == EventType.CALL_MISSED) && m_boolForwardToEMailMC ) {    				 
				 sendEMail("SMS Answering Machine: missed call from " + phoneNumber, "Not sending replies. Only forwarding to e mail.");
			 } else if ( (commEvent.getType() == EventType.SMS_RECEIVED) &&  m_boolForwardToEMailTextM) {    			
				 sendEMail("SMS Answering Machine: SMS received from " + phoneNumber, "Message: "+ commEvent.getMessage() +  " \n\nNot sending replies. Only forwarding to e mail");
			 }
		 }
     }
     
     private void addMessageToOutbox(String phone, String message) {
    	 ContentValues values = new ContentValues();
    	 values.put("address", phone);
    	 values.put("body", message);
    	 getContentResolver().insert(Uri.parse("content://sms/sent"), values);
     }

     private String getMyPhoneNumber() {
    	 String result = null;
    	 
    	 TelephonyManager telMgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
    	 result = telMgr.getLine1Number();
    	 
    	 return result;
     }      
     
     private synchronized void readPreferencesFromIntentsBundle(Intent intent) {
    	 if (intent != null) {
	    	 Bundle bundle = intent.getExtras();
	    	 if (bundle != null) {
				 m_strAnswer = (String) bundle.getCharSequence(mainActivity.PASS_DATA_ANSWER_TEXT);
				 m_boolAnserSMS = bundle.getBoolean(mainActivity.PASS_DATA_ANSWER_SMS);
				 m_boolAnserMC = bundle.getBoolean(mainActivity.PASS_DATA_ANSWER_MISSED_CALLS);				 
				 
				 m_boolHigherPriority = bundle.getBoolean(mainActivity.PREFS_SETTINGS_PRIORITY);
				 
				 m_intTimePeriod = bundle.getInt(mainActivity.PREFS_SETTINGS_TIME_PERIOD);
				 m_strLandLinePrefixes = bundle.getString(mainActivity.PREFS_SETTINGS_LAND_LINES_PREFIXES);
				 m_boolReplyToEveryone = bundle.getBoolean(mainActivity.PREFS_SETTINGS_REPLY_TO_EVERYONE);
				 m_boolReplyToContacts = bundle.getBoolean(mainActivity.PREFS_SETTINGS_REPLY_TO_CONTACTS);
				 m_boolReplyToNotContacts = bundle.getBoolean(mainActivity.PREFS_SETTINGS_REPLY_TO_NOT_CONTACTS);
				 
				 m_strEMailToForwardTo = bundle.getString(mainActivity.PREFS_SETTINGS_FORWARD_TO_THIS_EMAIL);
		         m_boolForwardToEMailMC = bundle.getBoolean(mainActivity.PREFS_SETTINGS_FORWARD_TO_EMAIL_MC);
		         m_boolForwardToEMailTextM = bundle.getBoolean(mainActivity.PREFS_SETTINGS_FORWARD_TO_EMAIL_TEXT_M);
		         
		         m_boolOnlyEMailForwarding = bundle.getBoolean(mainActivity.PREFS_SETTINGS_FORWARD_DONT_SEND_SMS_JUST_EMAILS);		         		         
				 
				 m_intRingerTypeWhileActive = bundle.getInt(mainActivity.PREFS_SETTINGS_RINGER_MODE);
				 
				 m_strHost = bundle.getString(mainActivity.PREFS_SETTINGS_SMTP_HOST);
		         m_strPort = bundle.getString(mainActivity.PREFS_SETTINGS_SMTP_PORT);
		         m_strUsername = bundle.getString(mainActivity.PREFS_SETTINGS_SMTP_USERNAME);
		         m_strPassword = bundle.getString(mainActivity.PREFS_SETTINGS_SMTP_PASSWORD);
		        	
			 } else {
				 Log.e(LOG_TAG, "read prefs from bundle, bundle is null!");
			 }
		 }    	 
     }
     
     private synchronized void purgeCallHistory() {
    	 if (m_hashMapCallersList != null) {
    		 Log.i(AnsweringMachineService.LOG_TAG, "Purging callers/sms senders history.");
	    	 Set<Entry<String, Long>> entries = m_hashMapCallersList.entrySet();
	    	 List<String> lstDelKeys = new ArrayList<String>();
	    	 
	    	 synchronized (entries) {
	    		 for (Entry<String, Long> entry : entries) {
		    		 String key = entry.getKey();
		    		 Long value = entry.getValue();
		    		 
		    		 Calendar calendar = Calendar.getInstance();
		    		 long now = calendar.getTimeInMillis();
		    		 long delta = now - value.longValue();
		    		 
		    		 long deltaMax = m_intTimePeriod * 3600000;
		    
		    		 Log.i(AnsweringMachineService.LOG_TAG, "number: " + key + "delta ms: " + delta + "delta max: " + deltaMax);
		    		 
		    		 if (delta > deltaMax) {
		    			 Log.i(AnsweringMachineService.LOG_TAG, "delta > deltaMax, deleting number from hash");		    			 
		    			 lstDelKeys.add(key); //add to list
		    		 }
		    	 }
			}
	    	 
	    	 //delete keys from hash map
	    	 Iterator<String> iterLst = lstDelKeys.iterator();
	    	 while (iterLst.hasNext()) {
	    		 String next = (String)iterLst.next();
	    		 m_hashMapCallersList.remove(next);
	    	 }
    	 }
     }
          
     public boolean contactExists(Context context, String number) {
    	/// number is the phone number
    	 boolean result = false;
    	Uri lookupUri = 
    			Uri.withAppendedPath(
    					PhoneLookup.CONTENT_FILTER_URI, 
    					Uri.encode(number)
    			);
    	String[] mPhoneNumberProjection = { PhoneLookup._ID, PhoneLookup.NUMBER, PhoneLookup.DISPLAY_NAME };
    	Cursor cur = context.getContentResolver().query(lookupUri,mPhoneNumberProjection, null, null, null);
    	try {
    	   if (cur.moveToFirst()) {
    	      result = true;
    	   }
    	} finally {
    		if (cur != null) {
    				cur.close(); 
    			}
    	}
    	
    	return result;
    	}


	public void onEvent(CommunicationEvent e) {
		if (e.getType() == EventType.SMS_RECEIVED || e.getType() == EventType.CALL_MISSED) {
			Log.i(AnsweringMachineService.LOG_TAG, "Service: event received.");
			String address = e.getAddress();
			replyWithSMSToPhoneNumber(address, e); 						
		}
	}
	
    private void cancelAutoShutDownSchedule() {
    	Intent intent = new Intent(this, AlarmReceiver.class);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 192838, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
		am.cancel(pendingIntent);
    }

    
    private void setRingerMode(int type) {
    	AudioManager audMangr;
    	audMangr= (AudioManager) getBaseContext().getSystemService(Context.AUDIO_SERVICE);    	    	
    	audMangr.setRingerMode(type);    	
    }
        
    private void readAndSetCurrentRingerType() {
    	AudioManager audMangr;
    	audMangr= (AudioManager) getBaseContext().getSystemService(Context.AUDIO_SERVICE);
    	m_intRingerTypeOld = audMangr.getRingerMode();
    }
    
    private boolean sendEMail(String strEMailSubject, String strEMailBody) {
    	boolean result = false;
    	    	
    	result = EMailHelper.sendEMail(AnsweringMachineService.this,m_strHost, m_strPort, 
    			m_strUsername, m_strPassword, m_strEMailToForwardTo, strEMailSubject, strEMailBody);
    	
    	return result;
    }
    
    private boolean isValidNumber(String strNumber) {
    	boolean result = true;
    	
    	if (strNumber.length() < 1) {
    		result = false;
    	}
    	
    	if (result) {
    		char chFirst = strNumber.charAt(0);
    		if ( !(chFirst == '+' || Character.isDigit(chFirst)) ) {
    			result = false;
    		}
    	}
    	
    	if (result) {
    		for (int i = 1; i < strNumber.length(); i++) {
    			if (!Character.isDigit(strNumber.charAt(i))) {
    				result = false;
    				break;
    			}
    		}
    	}    	
    	
    	return result;
    }
  
}
