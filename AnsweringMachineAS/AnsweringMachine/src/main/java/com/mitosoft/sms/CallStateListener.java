package com.mitosoft.sms;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.mitosoft.event.CommunicationEvent;
import com.mitosoft.event.EventType;
import com.mitosoft.event.ICommEventListener;

import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

class CallStateListener extends PhoneStateListener {
	
	final static String TAG = "STATELISTENER";
	boolean ringing = false;
	boolean offhook = false;
	
	private List m_eventListeners = new ArrayList();
	
	public void addEventListener(ICommEventListener l) {
		m_eventListeners.add(l);
	}
	
	public void removeEventListener(ICommEventListener l) {
		m_eventListeners.remove(l);
	}
	
	private void fireEvent(CommunicationEvent e) {
		Iterator listeners = m_eventListeners.iterator();
		while (listeners.hasNext()) {
			( (ICommEventListener) listeners.next() ).onEvent(e);
		}
	}	
	

	public void onCallStateChanged(int state, String incomingNumber) {			
	switch (state) {
		case TelephonyManager.CALL_STATE_IDLE:
			Log.d(TAG,"IDLE");		
			Log.d(TAG,"ringing "+ringing+" offhook "+offhook);
			if(ringing&&(!offhook)) {
				Log.i(AnsweringMachineService.LOG_TAG, "Missed call. Firing event.");								
				
                CommunicationEvent event = new CommunicationEvent(this,EventType.CALL_MISSED, incomingNumber, "missed call");
                fireEvent(event);
                
                //mozda bi ovo trebalo van if-a
				ringing = false;
				offhook = false;			
			}
			break;

		case TelephonyManager.CALL_STATE_RINGING:
			Log.i(AnsweringMachineService.LOG_TAG, "Phone is ringing.");		
			ringing = true;
			offhook = false;			
			break;
			
		case TelephonyManager.CALL_STATE_OFFHOOK:			
			offhook = true;
			ringing = false;
			break;

		default:						
			break;
		}			
	}		
}