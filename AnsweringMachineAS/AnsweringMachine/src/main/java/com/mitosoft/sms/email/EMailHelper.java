package com.mitosoft.sms.email;

import com.mitosoft.sms.AnsweringMachineService;
import com.mitosoft.sms.mainActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;


/**
 * Util class containing e mail related functions.
 * */
public final class EMailHelper {

	public static boolean sendEMail(Context context, String host, String port, String username, String password, String strEMailAddress, String strSubject, String strEMailBody) {
		boolean result = false;
		
		if (context == null) {
			Log.e(AnsweringMachineService.LOG_TAG, "EMailHelper.java: sendEMail(): context is null!");
		}
		
		if (isOnline(context)) {
			Mail mail = new Mail (username, password);
			String[] toArr = { strEMailAddress };
			
			mail.setHost(host);
			mail.setPort(port);
			
			mail.setTo(toArr);
			mail.setFrom(username);
			mail.setSubject(strSubject);
			mail.setBody(strEMailBody);
			
			 try { 
			        //mail.addAttachment("/sdcard/filelocation"); 		
			        if(mail.send()) { 
			          Toast.makeText(context, "Answering Machine: Email was sent successfully.", Toast.LENGTH_LONG).show();
			          Log.i(AnsweringMachineService.LOG_TAG, "EMailHelper.java: E mail sent successfully!");
			        } else { 
			          Toast.makeText(context, "Answering Machine: Email was not sent.", Toast.LENGTH_LONG).show();
			          Log.e(AnsweringMachineService.LOG_TAG, "EMailHelper.java: Failed to send e mail!");
			        } 
			      } catch(Exception e) { 
			        Toast.makeText(context, "Answering Machine: There was a problem sending the email.", Toast.LENGTH_LONG).show(); 
			        Log.e(AnsweringMachineService.LOG_TAG, "EMailHelper.java: Could not send email. Ex: " + e.getMessage(), e); 
			      } 
			    
		} else {
			Toast.makeText(context, "Answering Machine: Not online!", Toast.LENGTH_LONG).show();
	        Log.i(AnsweringMachineService.LOG_TAG, "EMailHelper.java: Not online, not sending e mail!");
		}
		return result;
	}
	
	public static boolean isOnline(Context context ) {
		boolean result = false;
		
		ConnectivityManager cm = null;
		
		if (context != null) {						        
			cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);			
		}
	    	    
	    if (cm != null) {
	    	result = cm.getActiveNetworkInfo().isConnectedOrConnecting(); 
	    }
	    
	    
	    return result;
	}

}
