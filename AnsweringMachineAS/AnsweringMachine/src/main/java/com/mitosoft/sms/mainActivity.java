package com.mitosoft.sms;

import java.util.Calendar;
import java.util.TimeZone;

import com.mitosoft.am.db.AMDB;
import com.mitosoft.am.db.Constants;
import com.mitosoft.answeringmachine.alarms.AlarmReceiver;
import com.mitosoft.logs.Logs;
import com.mitosoft.sms.email.SendEMail;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

public class mainActivity extends Activity {
	
	public static final String PREFS_NAME = "SMSAnsweringMachinePrefsFile";
	
	public static final String PASS_DATA_ANSWER_TEXT = "answerText";
	public static final String PASS_DATA_ANSWER_MISSED_CALLS = "answerMissedCalls";
	public static final String PASS_DATA_ANSWER_SMS = "answerSMS";
	
	public static final String PREFS_SETTINGS_TIME_PERIOD = "timePeriod";
	public static final String PREFS_SETTINGS_LAND_LINES_PREFIXES = "landLinesPrefixes";
	public static final String PREFS_SETTINGS_REPLY_TO_EVERYONE = "replyToEveryone";
	public static final String PREFS_SETTINGS_REPLY_TO_CONTACTS = "replyToContacts";
	public static final String PREFS_SETTINGS_REPLY_TO_SELECTED_CONTACTS = "replyToSelectedContacts";
	public static final String PREFS_SETTINGS_REPLY_TO_NOT_CONTACTS = "replyToNotontacts";
	public static final String PREFS_SETTINGS_RINGER_MODE = "ringerMode";
	
	public static final String PREFS_SETTINGS_FORWARD_TO_EMAIL_MC = "forwardToEMailMC";
	public static final String PREFS_SETTINGS_FORWARD_TO_EMAIL_TEXT_M = "forwardToEMailTextM";
	public static final String PREFS_SETTINGS_FORWARD_TO_THIS_EMAIL = "forwardToThisEMail";
	public static final String PREFS_SETTINGS_FORWARD_DONT_SEND_SMS_JUST_EMAILS = "doNotSendSMSJustForwardMeTheEmails";
	
	public static final String PREFS_SETTINGS_SMTP_HOST = "smtpHost";
	public static final String PREFS_SETTINGS_SMTP_PORT = "smtpPort";
	public static final String PREFS_SETTINGS_SMTP_USERNAME = "smtpUsername";
	public static final String PREFS_SETTINGS_SMTP_PASSWORD = "smtpPassword";
	
	public static final String PREFS_SETTINGS_PRIORITY = "checkBoxPriority";
	
	private static final String PREFS_LAST_REPLY_MESSAGE = "lastReplyMessage";
	private static final String PREFS_LAST_SPINNER_SELECTED_INDEX = "lastSpinnerSelectedIndex";
	private static final String PREFS_AUTO_SHUT_DOWN_SCHEDULED = "autoshutdownisscheduled";
	private static final String PREFS_AUTO_SHUT_DOWN_TIME= "autoshutdowntime";
	
	private static final String PREFS_AUTO_START_SCHEDULED = "autostartisscheduled";
	private static final String PREFS_AUTO_START_TIME= "autostarttime";
	
	private final int ACTIVITY_RESULT_SETTINGS_ID = 99;
	private static final int DATE_PICKER_AUTO_SHUT_DOWN_DIALOG_ID = 0;
	private static final int TIME_PICKER_AUTO_SHUT_DOWN_DIALOG_ID = 1;
	private static final int DATE_PICKER_AUTO_START_DIALOG_ID = 2;
	private static final int TIME_PICKER_AUTO_START_DIALOG_ID = 3;
	
	private CheckBox m_chkBoxMissedCalls = null;
	private CheckBox m_chkBoxSMS = null;
	private EditText m_txtAnswer = null;
	private Button m_btnStartStop = null;
	private TextView m_txtViewStatus = null;
	
	
	private Button m_btnAdd = null;
	private Button m_btnDelete = null;
	private com.mitosoft.ui.widgets.NoDefaultSpinner m_spnTemplates = null;
	
	
	private String m_strAnswerText = null;
	private boolean m_boolAnswerToSMS = false;
	private boolean m_boolAnswerToMC = false;
	
	private AMDB m_db = null; 
	private SimpleCursorAdapter m_messagesAdapter = null;
	private Cursor m_messagesCursor = null;
	
	private int m_intSpinnerInitiCount = 0;
	private static final int NO_OF_EVENTS = 1;
	private boolean m_boolListDeletingInProgress = false;
	
	private Button m_btnShutdownDate = null;
	private Button m_btnShutDownTime = null; 
	private ToggleButton m_tglBtnAutoShutDown = null;	
	private DatePickerDialog.OnDateSetListener m_dateSetListenerAutoShutDown = null;
	private TimePickerDialog.OnTimeSetListener m_timeSetListenerAutoShutDown = null;	
	private Calendar m_calAutoShutDownCalendar = null;
	
	private Button m_btnAutoStartDate = null;
	private Button m_btnAutoStartTime = null; 
	private ToggleButton m_tglBtnAutoStart = null;
	private DatePickerDialog.OnDateSetListener m_dateSetListenerAutoStart = null;
	private TimePickerDialog.OnTimeSetListener m_timeSetListenerAutoStart = null;
	private Calendar m_calAutoStartCalendar = null;	
	
	private static final String ACTION_ALARM_SHUTDOWN_EVENT = "com.mitosoft.am.ALARM_SHUTDOWN_EVENT";
	private static final String ACTION_ALARM_START_EVENT = "com.mitosoft.am.ALARM_START_EVENT";
	
	private IntentFilter m_intentFilterAutoShutDown = new IntentFilter();
	
	
	private BroadcastReceiver m_rcvrBroadcastAutoShutDown = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {			
			Toast.makeText(context, (String)"Turning Answering Machine Service Off!", Toast.LENGTH_LONG).show();
			Log.d(AnsweringMachineService.LOG_TAG, "Scheduler kicked off: Updating UI!");
			
			if (m_tglBtnAutoShutDown != null) {
				m_tglBtnAutoShutDown.setChecked(false);
				setStatusLabelAndButtonText(false);
				setScheduledAutoShutDown(false, -1);
			}
		}
	};
	
	private IntentFilter m_intentFilterAutoStart = new IntentFilter();
	private BroadcastReceiver m_rcvrBroadcastAutoStart = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {			
			Toast.makeText(context, (String)"Turning Answering Machine Service On!", Toast.LENGTH_LONG).show();
			Log.d(AnsweringMachineService.LOG_TAG, "Scheduler kicked off: Updating UI!");
			
			if (m_tglBtnAutoStart != null) {
				m_tglBtnAutoStart.setChecked(false);
				setStatusLabelAndButtonText(true);
				setAutoStartScheduled(false, -1);
			}
		}
	};
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
                
        m_chkBoxMissedCalls = (CheckBox)findViewById(R.id.checkBoxMC);
        m_chkBoxSMS = (CheckBox)findViewById(R.id.checkBoxSMS);
        m_txtAnswer = (EditText)findViewById(R.id.editTextAnswer);
        m_btnStartStop = (Button)findViewById(R.id.buttonStartStop);
        m_txtViewStatus = (TextView)findViewById(R.id.textViewStatus);
        m_btnAdd = (Button)findViewById(R.id.btnAdd);
        m_btnDelete = (Button)findViewById(R.id.btnDelete);
        m_spnTemplates = (com.mitosoft.ui.widgets.NoDefaultSpinner) findViewById(R.id.spnTemplates);
        m_btnShutdownDate = (Button)findViewById(R.id.btnDate);
        m_btnShutDownTime = (Button)findViewById(R.id.btnTime);
        m_tglBtnAutoShutDown = (ToggleButton)findViewById(R.id.tglButtonAutoShutDown);
        m_btnAutoStartDate = (Button)findViewById(R.id.btnAutoStartDate);
        m_btnAutoStartTime = (Button)findViewById(R.id.btnAutoStartTime);
        m_tglBtnAutoStart = (ToggleButton)findViewById(R.id.tglBtnAutoStart);
                
        m_calAutoShutDownCalendar = Calendar.getInstance();
        m_calAutoStartCalendar = Calendar.getInstance();
        
        m_intentFilterAutoShutDown.addAction(ACTION_ALARM_SHUTDOWN_EVENT); 
        m_intentFilterAutoStart.addAction(ACTION_ALARM_START_EVENT);
        
        m_db = new AMDB(this.getApplicationContext());   
        m_db.open();
        fillSpinnerWithData();
        
        // Restore preferences
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        if (settings != null) {
	        m_strAnswerText = settings.getString(PASS_DATA_ANSWER_TEXT, "Answering machine: I will call you later.");
	        m_boolAnswerToMC = settings.getBoolean(PASS_DATA_ANSWER_MISSED_CALLS, false);
	        m_boolAnswerToSMS = settings.getBoolean(PASS_DATA_ANSWER_SMS, false);                
	
	        m_chkBoxMissedCalls.setChecked(m_boolAnswerToMC);
	        m_chkBoxSMS.setChecked(m_boolAnswerToSMS);
	        m_txtAnswer.setText(m_strAnswerText); 
        }
        
        setStatusLabelAndButtonText(isAnsweringMachineServiceRunning());       
                
        m_btnStartStop.setOnClickListener(new Button.OnClickListener() {						
			public void onClick(View v) {	//to do refactor to use createServiceIntent() method			
				if (!isAnsweringMachineServiceRunning()) {
					String strAnswer = m_txtAnswer.getText().toString();
					boolean boolAnswerMissedCalls = m_chkBoxMissedCalls.isChecked();
					boolean boolAnswerSMS = m_chkBoxSMS.isChecked();
					
					
					//first check if we will only do e mail forwarding
					boolean boolEMailForwardingOn = isEMailForwardingOn();
					if ( (!boolAnswerMissedCalls) && (!boolAnswerSMS) && boolEMailForwardingOn) {
						Toast.makeText(getApplicationContext(), "I will only forward missed calls notifications OR text messages to e mail.", Toast.LENGTH_LONG).show();
						
						Intent serviceIntent = new Intent(mainActivity.this,AnsweringMachineService.class); 
						
						Bundle bundle = new Bundle();
						bundle.putCharSequence(mainActivity.PASS_DATA_ANSWER_TEXT, "e mail forwarding");
						bundle.putBoolean(mainActivity.PASS_DATA_ANSWER_MISSED_CALLS, false);
						bundle.putBoolean(mainActivity.PASS_DATA_ANSWER_SMS, false);
						bundle.putBoolean(mainActivity.PREFS_SETTINGS_FORWARD_DONT_SEND_SMS_JUST_EMAILS, true);
						
						addSettingsToBundle(bundle, mainActivity.this);
						
						serviceIntent.putExtras(bundle);
						
						startService(serviceIntent);
						setStatusLabelAndButtonText(true);
					}
					else if (!strAnswer.equalsIgnoreCase("\n\n") && strAnswer.length() > 0) {
						m_strAnswerText = strAnswer;
						m_boolAnswerToMC = boolAnswerMissedCalls;
						m_boolAnswerToSMS = boolAnswerSMS;
						if (boolAnswerMissedCalls || boolAnswerSMS) {
							
							Intent serviceIntent = new Intent(mainActivity.this,AnsweringMachineService.class); 
							
							Bundle bundle = new Bundle();
							bundle.putCharSequence(mainActivity.PASS_DATA_ANSWER_TEXT, strAnswer);
							bundle.putBoolean(mainActivity.PASS_DATA_ANSWER_MISSED_CALLS, boolAnswerMissedCalls);
							bundle.putBoolean(mainActivity.PASS_DATA_ANSWER_SMS, boolAnswerSMS);
						
							bundle.putBoolean(mainActivity.PREFS_SETTINGS_FORWARD_DONT_SEND_SMS_JUST_EMAILS, false);
							
							addSettingsToBundle(bundle, mainActivity.this);
							
							serviceIntent.putExtras(bundle);
							
							startService(serviceIntent);
							setStatusLabelAndButtonText(true); 							
						} else {
							Toast.makeText(getApplicationContext(), "Select what to answer.", Toast.LENGTH_LONG).show();
						}
					} else {
						Toast.makeText(getApplicationContext(), "Enter answer text.", Toast.LENGTH_LONG).show();
					}	
					
					
					
				} else {
					stopService(new Intent(mainActivity.this,AnsweringMachineService.class));
					setStatusLabelAndButtonText(false);
				}							
			}
		});
        
        m_btnAdd.setOnClickListener(new Button.OnClickListener() {						
			public void onClick(View v) {
				showAddTemplateDialogBox();
			}
		});
        
        m_btnDelete.setOnClickListener(new Button.OnClickListener() {						
			public void onClick(View v) {				
				showDeleteTemplateDialogBox();				
			}
		});
    
        
        m_spnTemplates.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
  			public void onItemSelected(AdapterView<?> parentView, 
  					View selectedItemView, int position, long id) { 
  				
  				//trying to avoid undesired spinner selection changed event, a known problem
  				//http://stackoverflow.com/questions/5624825/spinner-onitemselected-executes-when-it-is-not-suppose-to
  					if (m_intSpinnerInitiCount < NO_OF_EVENTS) {
  						m_intSpinnerInitiCount++;  						
  					} else if (m_boolListDeletingInProgress){ 
  						m_boolListDeletingInProgress = false;
  					}  	else {			
  						//View viewAtPos = (View)m_spnTemplates.getItemAtPosition(position);
		  				TextView tv = (TextView)selectedItemView.findViewById(R.id.tvDBViewRow);
		  				String text = tv.getText().toString().trim();
		  				m_txtAnswer.setText(text); 
	  				}   					
  					  					
  			}
  			
  			public void onNothingSelected(AdapterView<?> arg0) {
  				// TODO Auto-generated method stub  			
  				
  			}
  		});

        m_dateSetListenerAutoShutDown = new DatePickerDialog.OnDateSetListener() {
        	
				public void onDateSet(DatePicker arg0, int year, int month,
						int day) {
					Calendar cal = Calendar.getInstance();
					cal.set(Calendar.YEAR, year);
					cal.set(Calendar.MONTH, month);
					cal.set(Calendar.DAY_OF_MONTH, day);
					
					m_calAutoShutDownCalendar.set(Calendar.YEAR, year);
					m_calAutoShutDownCalendar.set(Calendar.MONTH, month);
					m_calAutoShutDownCalendar.set(Calendar.DAY_OF_MONTH, day);
					
					android.text.format.DateFormat df = new android.text.format.DateFormat();
					String date = (String) df.format("yyyy-MM-dd", cal);
			    	m_btnShutdownDate.setText(date);			    

				}
        	};
        	
        m_timeSetListenerAutoShutDown = new TimePickerDialog.OnTimeSetListener() {
        	
			public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
				Calendar cal = Calendar.getInstance();
				cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
				cal.set(Calendar.MINUTE, minute);
				
				m_calAutoShutDownCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
				m_calAutoShutDownCalendar.set(Calendar.MINUTE, minute);
				
		    	android.text.format.DateFormat df = new android.text.format.DateFormat();		    
		    	String time = (String) df.format("hh:mm A", cal);		    	
		    	m_btnShutDownTime.setText(time);		    
			}
		};
        
		
		m_dateSetListenerAutoStart = new DatePickerDialog.OnDateSetListener() {
			
			public void onDateSet(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {				
					Calendar cal = Calendar.getInstance();
					cal.set(Calendar.YEAR, year);
					cal.set(Calendar.MONTH, monthOfYear);
					cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
					
					m_calAutoStartCalendar.set(Calendar.YEAR, year);
					m_calAutoStartCalendar.set(Calendar.MONTH, monthOfYear);
					m_calAutoStartCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
					
					android.text.format.DateFormat df = new android.text.format.DateFormat();
					String date = (String) df.format("yyyy-MM-dd", m_calAutoStartCalendar);
			    	m_btnAutoStartDate.setText(date);				
			}
		};
		
		m_timeSetListenerAutoStart = new TimePickerDialog.OnTimeSetListener() {
			
			public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
				Calendar cal = Calendar.getInstance();
				cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
				cal.set(Calendar.MINUTE, minute);
				
				m_calAutoStartCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
				m_calAutoStartCalendar.set(Calendar.MINUTE, minute);
				m_calAutoStartCalendar.set(Calendar.SECOND, 0);
				m_calAutoStartCalendar.set(Calendar.MILLISECOND, 0);
				
		    	android.text.format.DateFormat df = new android.text.format.DateFormat();		    
		    	String time = (String) df.format("hh:mm A", m_calAutoStartCalendar);		    	
		    	m_btnAutoStartTime.setText(time);
				
			}
		};
		
        m_btnShutdownDate.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				showDialog(DATE_PICKER_AUTO_SHUT_DOWN_DIALOG_ID);
			}
		}) ;
        
        m_btnShutDownTime.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				showDialog(TIME_PICKER_AUTO_SHUT_DOWN_DIALOG_ID);
			}
		});
        
        m_btnAutoStartDate.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				showDialog(DATE_PICKER_AUTO_START_DIALOG_ID);
			}
		});
        
        m_btnAutoStartTime.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				showDialog(TIME_PICKER_AUTO_START_DIALOG_ID);
			}
		});
        
        m_tglBtnAutoShutDown.setOnClickListener(new View.OnClickListener() {
			
        	public void onClick(View v) {
        		boolean isChecked = m_tglBtnAutoShutDown.isChecked();
				Intent intent = new Intent(mainActivity.this, AutoShutDownReceiver.class);
				PendingIntent pendingIntent = PendingIntent.getBroadcast(mainActivity.this, 192837, intent, PendingIntent.FLAG_UPDATE_CURRENT);
				AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
				
				if (isChecked) {
					long scheduledTime = m_calAutoShutDownCalendar.getTimeInMillis();
					long currentTime = Calendar.getInstance().getTimeInMillis();
					if (scheduledTime > currentTime) {
						if (isAnsweringMachineServiceRunning() ||  (isAutoStartScheduled() && (m_calAutoShutDownCalendar.getTimeInMillis() > m_calAutoStartCalendar.getTimeInMillis()) ) ) {
							Toast.makeText(mainActivity.this, "Scheduling auto shut down!", Toast.LENGTH_LONG).show();
							am.set(AlarmManager.RTC_WAKEUP, m_calAutoShutDownCalendar.getTimeInMillis(), pendingIntent);
							

					        
					    	android.text.format.DateFormat df = new android.text.format.DateFormat();
					    	String date = (String) df.format("yyyy-MM-dd", m_calAutoShutDownCalendar);
					    	String time = (String) df.format("hh:mm A", m_calAutoShutDownCalendar);
					        Log.d(AnsweringMachineService.LOG_TAG, "Scheduling shut down for " + date + ": " + time);
							
							setScheduledAutoShutDown(true, m_calAutoShutDownCalendar.getTimeInMillis()); 
						} else {
							setScheduledAutoShutDown(false, -1);
							m_tglBtnAutoShutDown.setChecked(false);
							Toast.makeText(mainActivity.this, "Answering Machine Service not running or auto start not scheduled or :-) time needs to be > then auto start!", Toast.LENGTH_LONG).show();
						}
					} else {
						Toast.makeText(mainActivity.this, "Time expired!", Toast.LENGTH_LONG).show();
						am.cancel(pendingIntent);
						setScheduledAutoShutDown(false, -1);
						m_tglBtnAutoShutDown.setChecked(false);
					}						
				} else {
					//turn off
					Toast.makeText(mainActivity.this, "Canceling auto shut down!", Toast.LENGTH_LONG).show();
					am.cancel(pendingIntent);			
					setScheduledAutoShutDown(false, -1);
					m_tglBtnAutoShutDown.setChecked(false);
				}
			}
		});
        
        
        m_tglBtnAutoStart.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
        		boolean isChecked = m_tglBtnAutoStart.isChecked();
				Intent intent = new Intent(mainActivity.this, AutoStartReceiver.class);
				if (intent != null) {
					PendingIntent pendingIntent = PendingIntent.getBroadcast(mainActivity.this, 192844, intent, PendingIntent.FLAG_UPDATE_CURRENT);
					AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
									
					if (isChecked) {
						long scheduledTime = m_calAutoStartCalendar.getTimeInMillis();
						long currentTime = Calendar.getInstance().getTimeInMillis();
						if (scheduledTime > currentTime) {
							
							
							am.set(AlarmManager.RTC_WAKEUP, m_calAutoStartCalendar.getTimeInMillis(), pendingIntent);
							
					    	android.text.format.DateFormat df = new android.text.format.DateFormat();
					    	String date = (String) df.format("yyyy-MM-dd", m_calAutoStartCalendar);
					    	String time = (String) df.format("hh:mm A", m_calAutoStartCalendar);
					        Log.d(AnsweringMachineService.LOG_TAG, "Scheduling auto start for " + date + ": " + time);
					        Toast.makeText(mainActivity.this, "Scheduling start for: " + date + " : " + time+ " with current settings!", Toast.LENGTH_LONG).show();
							
					        setAutoStartScheduled(true, m_calAutoStartCalendar.getTimeInMillis());		
							
							//store all settings to bundle, after auto start receiver gets activated, it will read these preferences and 
							//launch the app
							savePrefs();
							saveLastReplyMessage(m_txtAnswer.getText().toString().trim());
							
						} else {
							Toast.makeText(mainActivity.this, "Time expired!", Toast.LENGTH_LONG).show();
							am.cancel(pendingIntent);
							setAutoStartScheduled(false, -1);
							m_tglBtnAutoStart.setChecked(false);
						}						
					} else {
						//turn off
						Toast.makeText(mainActivity.this, "Canceling auto start!", Toast.LENGTH_LONG).show();
						am.cancel(pendingIntent);
						setAutoStartScheduled(false, -1);
						m_tglBtnAutoStart.setChecked(false);
				}				
				}
			}
		});
		
        initAutoShutDownDateTime();
        initAutoStartDateTime();
        restoreLastReplyMessage();
        restoreAutoShutDownToggleButtonState();
        restoreAutoStartToggleButtonState();
    }    
        
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }
    
    @Override
    protected Dialog onCreateDialog(int id) {  
    	Dialog result = null;
    	final Calendar c = Calendar.getInstance();
    	
    	switch(id) {
    		case DATE_PICKER_AUTO_SHUT_DOWN_DIALOG_ID:    		
    			int year = c.get(Calendar.YEAR);
    			int month = c.get(Calendar.MONTH);
    		    int day = c.get(Calendar.DAY_OF_MONTH);

    			result = new DatePickerDialog(this,
                        m_dateSetListenerAutoShutDown,
                        year, month, day);
    			break;
    	
    		case TIME_PICKER_AUTO_SHUT_DOWN_DIALOG_ID:    		
    			int hours = c.get(Calendar.HOUR_OF_DAY);
    			int minutes = c.get(Calendar.MINUTE);
    			result = new TimePickerDialog(this, 
    					m_timeSetListenerAutoShutDown, 
    					hours, minutes, false);    			
    			break;    			
    		case DATE_PICKER_AUTO_START_DIALOG_ID:
    			int yearStart = c.get(Calendar.YEAR);
    			int monthStart = c.get(Calendar.MONTH);
    		    int dayStart = c.get(Calendar.DAY_OF_MONTH);

    			result = new DatePickerDialog(this,
                        m_dateSetListenerAutoStart,
                        yearStart, monthStart, dayStart);

    			break;
    		case TIME_PICKER_AUTO_START_DIALOG_ID:
    			int hoursStart = c.get(Calendar.HOUR_OF_DAY);
    			int minutesStart = c.get(Calendar.MINUTE);
    			result = new TimePickerDialog(this, 
    					m_timeSetListenerAutoStart, 
    					hoursStart, minutesStart, false);   
    			break;
    	}
    	
    	if (result == null) {
    		result = super.onCreateDialog(id);
    	}
    	
    	return result;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case R.id.menu_item_more_settings:  
        	Intent startSettingsActivity = new Intent(mainActivity.this, SettingsActivity.class);
        	startActivityForResult(startSettingsActivity, ACTIVITY_RESULT_SETTINGS_ID);
            return true;
        case R.id.menu_item_suggest_feature:    
        	Intent startEMailActivity = new Intent(mainActivity.this, SendEMail.class);
        	startActivity(startEMailActivity);
            return true;
        case R.id.menu_item_about:     
        	showInfoMessageBox();      
            return true;
        case R.id.menu_item_logs:
        	Toast.makeText(getApplicationContext(), "Please wait.", Toast.LENGTH_LONG).show();
        	Intent startLogsActivity = new Intent(mainActivity.this, Logs.class);
        	startActivity(startLogsActivity);
        	return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	
    	if (resultCode == Activity.RESULT_OK) {
    		if (requestCode == ACTIVITY_RESULT_SETTINGS_ID) {
    			Toast.makeText(this, "Please restart the service!", Toast.LENGTH_LONG).show();
    		}
    	}    	    	
    }
    
    private void setStatusLabelAndButtonText(boolean active) {
    	m_txtViewStatus.setTextColor(Color.WHITE);
    	if (active) {
    		m_txtViewStatus.setText("Active");
    		m_txtViewStatus.setBackgroundColor(Color.GREEN);
    		m_btnStartStop.setText("Stop service");
    	} else {
    		m_txtViewStatus.setText("Inactive");
    		m_txtViewStatus.setBackgroundColor(Color.rgb(255, 165, 0));
    		m_btnStartStop.setText("Start service");
    	}
    }
    
    private boolean isAnsweringMachineServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if ("com.mitosoft.sms.AnsweringMachineService".equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    
    @Override
    protected void onStop() {    	
    	super.onStop();
    	
    	try {
	    	m_intSpinnerInitiCount = 0;
	    	
	    	savePrefs();
	    	saveLastReplyMessage(m_txtAnswer.getText().toString().trim());
	    	
	    	//need to close cursor, adapter and db in proper order
	    	//http://stackoverflow.com/questions/4195089/what-does-invalid-statement-in-fillwindow-in-android-cursor-mean
	    	/** 
	    	 * When dealing with ListActivities, this issue has to do with the Cursor objects, CursorAdapter objects, and Database 
	    	 * objects not being closed properly when the Activity stops, and not being set properly when the Activity starts or resumes.
	    	 * I had to make sure that I closed my SimpleListAdapter, my Cursors, and then my Database objects in that respective order, 
	    	 * in the onStop method of the Activity that is called when the TabActivity resumes.I had already been closing the Cursor and 
	    	 * Database objects, but had not been closing my SimpleListAdapter Cursor.
	    	 * */
	    	if (m_messagesAdapter != null) {
	    		m_messagesAdapter.getCursor().close();
	    		m_messagesAdapter = null;
	    	}
	    	
	    	if (m_messagesCursor != null) {
	    		m_messagesCursor.close();
	    		m_messagesCursor = null;
	    	}
	    	
	    	if (m_db != null) {
	    		m_db.close();
	    	}
    	} catch (Exception ex) {
    		Log.d(AnsweringMachineService.LOG_TAG, "A crash happened in the onStop(): " + ex.getMessage());
    		Toast.makeText(this, "Error while closing the app. Please send logs to the developer.",Toast.LENGTH_LONG).show();
    	}
    }
    
    
    @Override
    protected void onStart() {    
    	super.onStart();      	    	
    }
    
    @Override
    protected void onResume() {    	
    	super.onResume();
    	
    	registerReceiver(m_rcvrBroadcastAutoShutDown, m_intentFilterAutoShutDown);
    	registerReceiver(m_rcvrBroadcastAutoStart, m_intentFilterAutoStart);

    	
    	if (m_db != null) {
    		m_db.open();
    	}
    	    	
    	fillSpinnerWithData();
    	refreshData();
    }
    
    @Override
    protected void onPause() {
    	unregisterReceiver(m_rcvrBroadcastAutoShutDown);
    	unregisterReceiver(m_rcvrBroadcastAutoStart);
    	super.onPause();
    }
    
    private void savePrefs() {
    	// We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        
        m_strAnswerText = m_txtAnswer.getText().toString();
        m_boolAnswerToMC = m_chkBoxMissedCalls.isChecked();
        m_boolAnswerToSMS = m_chkBoxSMS.isChecked();
        
        editor.putString(PASS_DATA_ANSWER_TEXT, m_strAnswerText);
        editor.putBoolean(PASS_DATA_ANSWER_MISSED_CALLS, m_boolAnswerToMC);
        editor.putBoolean(PASS_DATA_ANSWER_SMS, m_boolAnswerToSMS);

        // Commit the edits!
        editor.commit();
    }        
    
    
    private void showInfoMessageBox() {
    	LayoutInflater inflater = getLayoutInflater();
		final View dialoglayout = inflater.inflate(R.layout.infomessagebox, (ViewGroup) findViewById(R.id.layout_info));
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setIcon(R.drawable.ic_info);
		builder.setTitle("About");		
		builder.setView(dialoglayout);
	
		builder.setPositiveButton("Close", new OnClickListener() {
									
			public void onClick(DialogInterface arg0, int arg1) {
				// TODO Auto-generated method stub

			}
		});
		
		builder.show();
    }
    
    private static void addSettingsToBundle(Bundle bundle, Context ctx) {
    	 // Restore preferences
        SharedPreferences settings = ctx.getSharedPreferences(mainActivity.PREFS_NAME, 0);
        if (settings != null) {	       
        	int spinnerVal = settings.getInt(mainActivity.PREFS_SETTINGS_TIME_PERIOD, 1);        	
        	String landLinePrefixes = settings.getString(mainActivity.PREFS_SETTINGS_LAND_LINES_PREFIXES, "+38521,");        	        	
        	boolean boolEveryone = settings.getBoolean(mainActivity.PREFS_SETTINGS_REPLY_TO_EVERYONE, true);        	
        	boolean boolContacts = settings.getBoolean(mainActivity.PREFS_SETTINGS_REPLY_TO_CONTACTS, false);
        	boolean boolNotContacts = settings.getBoolean(mainActivity.PREFS_SETTINGS_REPLY_TO_NOT_CONTACTS, false); 
        	boolean boolSelectedContacts = settings.getBoolean(mainActivity.PREFS_SETTINGS_REPLY_TO_SELECTED_CONTACTS, false);
        	int intRingerMode = settings.getInt(mainActivity.PREFS_SETTINGS_RINGER_MODE, AudioManager.RINGER_MODE_NORMAL);
        	
        	boolean boolHigherPriority = settings.getBoolean(mainActivity.PREFS_SETTINGS_PRIORITY, false);
        	
        	String strEMailToForwardTo = settings.getString(mainActivity.PREFS_SETTINGS_FORWARD_TO_THIS_EMAIL, "n/a");
        	boolean boolForwardToEMailMC = settings.getBoolean(mainActivity.PREFS_SETTINGS_FORWARD_TO_EMAIL_MC, false);
        	boolean boolForwardToEMailTextM = settings.getBoolean(mainActivity.PREFS_SETTINGS_FORWARD_TO_EMAIL_TEXT_M, false);
        	
        	bundle.putString(PREFS_SETTINGS_FORWARD_TO_THIS_EMAIL, strEMailToForwardTo);
        	bundle.putBoolean(PREFS_SETTINGS_FORWARD_TO_EMAIL_MC, boolForwardToEMailMC);
        	bundle.putBoolean(PREFS_SETTINGS_FORWARD_TO_EMAIL_TEXT_M, boolForwardToEMailTextM);
        	        	
        	String strSMTPHost = settings.getString(mainActivity.PREFS_SETTINGS_SMTP_HOST, "smtp.gmail.com");
        	String strSMTPPort = settings.getString(mainActivity.PREFS_SETTINGS_SMTP_PORT, "465");
        	String strSMTUsername = settings.getString(mainActivity.PREFS_SETTINGS_SMTP_USERNAME, "n/a");
        	String strSMTPPassword = settings.getString(mainActivity.PREFS_SETTINGS_SMTP_PASSWORD, "n/a");
        	
        	bundle.putString(PREFS_SETTINGS_SMTP_HOST, strSMTPHost);
        	bundle.putString(PREFS_SETTINGS_SMTP_PORT, strSMTPPort);
        	bundle.putString(PREFS_SETTINGS_SMTP_USERNAME, strSMTUsername);
        	bundle.putString(PREFS_SETTINGS_SMTP_PASSWORD, strSMTPPassword);
        	
        	bundle.putInt(PREFS_SETTINGS_TIME_PERIOD, spinnerVal);
        	bundle.putString(PREFS_SETTINGS_LAND_LINES_PREFIXES, landLinePrefixes);
        	bundle.putBoolean(PREFS_SETTINGS_REPLY_TO_EVERYONE, boolEveryone);
        	bundle.putBoolean(PREFS_SETTINGS_REPLY_TO_CONTACTS, boolContacts);
        	bundle.putBoolean(PREFS_SETTINGS_REPLY_TO_NOT_CONTACTS, boolNotContacts);
        	bundle.putBoolean(PREFS_SETTINGS_REPLY_TO_SELECTED_CONTACTS, boolSelectedContacts);
        	bundle.putBoolean(PREFS_SETTINGS_PRIORITY, boolHigherPriority);
        	bundle.putInt(PREFS_SETTINGS_RINGER_MODE, intRingerMode);
        	//TODO selected contacts
        	
        	/*String strSettings = "Settings: Time delay: " + spinnerVal + " landLinePrefixes: " + landLinePrefixes + " reply to everyone: " + boolEveryone + 
        								" reply to contacts: " + boolContacts + " reply to selected contacts: " + boolSelectedContacts;        
        	Toast.makeText(this, strSettings, Toast.LENGTH_LONG).show();*/
        }       
    }
    
    private void showAddTemplateDialogBox() {
    	DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
    	    public void onClick(DialogInterface dialog, int which) {
    	        switch (which){
    	        case DialogInterface.BUTTON_POSITIVE:
    	            
    	        	String enteredMessage = m_txtAnswer.getText().toString().trim();
    	        	if (enteredMessage.length() > 0) {
    	        		if (m_db != null) {
    	        			m_db.open();
    	        			boolean isInDatabase = m_db.isMessageInTheDatabase(enteredMessage);
    	        			if (isInDatabase) {
    	        				Toast.makeText(mainActivity.this, "Already in the templates.", Toast.LENGTH_LONG).show();
    	        			} else {    	        				    	        				
    	        				m_db.insertMessage(enteredMessage);        	        				    	        				
    	        				refreshData(); 	        				
    	        			}    	        			
    	        		}
    	        	} else {
    	        		Toast.makeText(mainActivity.this, "Enter message first.", Toast.LENGTH_LONG).show();
    	        	}
    	        	
    	            break;

    	        case DialogInterface.BUTTON_NEGATIVE:
    	            //No button clicked
    	            break;
    	        }
    	    }
    	};

    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setMessage("Add current reply message to templates?");
    	builder.setPositiveButton("Yes", dialogClickListener);
    	builder.setNegativeButton("No", dialogClickListener);
    	builder.show();
    }
    
    private void showDeleteTemplateDialogBox() {
    	DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
    	    public void onClick(DialogInterface dialog, int which) {
    	        switch (which){
    	        case DialogInterface.BUTTON_POSITIVE:
    	            
    	        	String enteredMessage = m_txtAnswer.getText().toString().trim();
    	        	if (enteredMessage.length() > 0) {
    	        		if (m_db != null) {    	        			
    	        			boolean isInDatabase = m_db.isMessageInTheDatabase(enteredMessage);
    	        			if (!isInDatabase) {
    	        				Toast.makeText(mainActivity.this, "Not in the templates.", Toast.LENGTH_LONG).show();
    	        			} else {
    	        				m_boolListDeletingInProgress = true;
    	        				long id = m_db.getMessageRowID(enteredMessage);    	        						
    	        				m_db.deleteMessage(id);
    	        				refreshData();
    	        			}    	        			
    	        		}
    	        	} else {
    	        		Toast.makeText(mainActivity.this, "Enter message first.", Toast.LENGTH_LONG).show();
    	        	}
    	        	
    	            break;

    	        case DialogInterface.BUTTON_NEGATIVE:
    	            //No button clicked
    	            break;
    	        }
    	    }
    	};

    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setMessage("Delete current reply message from templates?");
    	builder.setPositiveButton("Yes", dialogClickListener);
    	builder.setNegativeButton("No", dialogClickListener);
    	builder.show();
    }
    
    private void fillSpinnerWithData() {    	
    	if (m_db != null) {  
    		
    		if (m_messagesCursor == null) {
    			m_messagesCursor = m_db.getMessages();	
    			startManagingCursor(m_messagesCursor); 
	    	}
	    	
	    	if (m_messagesAdapter == null) {	    	
		    	/*Create an array to specify the fields we want to display in the list */
		    	String[] from = new String[]{Constants.FIELD_MESSAGE};
		    	
		    	/* and an array of the fields we want to bind those fields to */
		    	int[] to = new int[]{R.id.tvDBViewRow};
		
		    	/* Now create a simple cursor adapter.. */
		    	m_messagesAdapter =
		    	new SimpleCursorAdapter(this, R.layout.db_view_spinner_row, m_messagesCursor, from, to);
		    	
		    	
		    	/* and assign it to our Spinner widget */
		    	m_spnTemplates.setAdapter(m_messagesAdapter);
	    	}	    		    	
    	}
    }
    
    public void refreshData() {
    	if (m_messagesAdapter != null) {
	    	m_messagesCursor.requery();
	    	m_messagesAdapter.notifyDataSetChanged();	    	
    	}
    }
    
    private void saveLastReplyMessage(String message) {
    	SharedPreferences settings = getSharedPreferences(mainActivity.PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        
        String replyMessage = m_txtAnswer.getText().toString().trim();
        int spinnerIndex = m_spnTemplates.getSelectedItemPosition();
        
        if (replyMessage != null && replyMessage.length() > 0) {
        	editor.putString(mainActivity.PREFS_LAST_REPLY_MESSAGE, replyMessage);        	
        }        
        editor.putInt(mainActivity.PREFS_LAST_SPINNER_SELECTED_INDEX, spinnerIndex);
      
        // Commit the edits!
        editor.commit();
    }
    
    private void restoreLastReplyMessage() {    	
    	SharedPreferences settings = getSharedPreferences(mainActivity.PREFS_NAME, 0);
        
    	if (settings != null) {	        
        	String replyMessage = settings.getString(mainActivity.PREFS_LAST_REPLY_MESSAGE, "N/A");
        	int index = settings.getInt(mainActivity.PREFS_LAST_SPINNER_SELECTED_INDEX, -1);
        	if (replyMessage != null && replyMessage.length() > 0) {
        		m_txtAnswer.setText(replyMessage);        		        		      		        		
        	}
        	//if (index != -1) {//todo: nepotrebno sad kad imam custom adapter
        		//m_spnTemplates.setSelection(index);
        	//}
        }
    }
    
    private void initAutoShutDownDateTime() {
    	Calendar cal = Calendar.getInstance();
    	    	
    	if (isAutoShutDownScheduled()) {
    		SharedPreferences settings = getSharedPreferences(mainActivity.PREFS_NAME, 0);        	
        	long time = settings.getLong(PREFS_AUTO_SHUT_DOWN_TIME, -1);
        	if (time > -1) {
        		cal.setTimeInMillis(time);
        	}        	
    	}
    	
    	android.text.format.DateFormat df = new android.text.format.DateFormat();
    	String date = (String) df.format("yyyy-MM-dd", cal);
    	String time = (String) df.format("hh:mm A", cal);

    	int year = cal.get(Calendar.YEAR);
    	int month = cal.get(Calendar.MONTH);
    	int day = cal.get(Calendar.DAY_OF_MONTH);
    	int hour = cal.get(Calendar.HOUR_OF_DAY);
    	int minute = cal.get(Calendar.MINUTE);
    	
		m_calAutoShutDownCalendar.set(Calendar.YEAR, year);
		m_calAutoShutDownCalendar.set(Calendar.MONTH, month);
		m_calAutoShutDownCalendar.set(Calendar.DAY_OF_MONTH, day);
		m_calAutoShutDownCalendar.set(Calendar.HOUR_OF_DAY, hour);
		m_calAutoShutDownCalendar.set(Calendar.MINUTE, minute);
		m_calAutoShutDownCalendar.set(Calendar.SECOND, 0);
		m_calAutoShutDownCalendar.set(Calendar.MILLISECOND, 0);
    	
    	m_btnShutdownDate.setText(date);
    	m_btnShutDownTime.setText(time);
    }
    
    private void initAutoStartDateTime() {
    	Calendar cal = Calendar.getInstance();

    	if (isAutoStartScheduled()) {
    		SharedPreferences settings = getSharedPreferences(mainActivity.PREFS_NAME, 0);        	
        	long time = settings.getLong(PREFS_AUTO_START_TIME, -1);
        	if (time > -1) {
        		cal.setTimeInMillis(time);
        	}        	
    	}
    	
    	android.text.format.DateFormat df = new android.text.format.DateFormat();
    	String date = (String) df.format("yyyy-MM-dd", cal);
    	String time = (String) df.format("hh:mm A", cal);

    	int year = cal.get(Calendar.YEAR);
    	int month = cal.get(Calendar.MONTH);
    	int day = cal.get(Calendar.DAY_OF_MONTH);
    	int hour = cal.get(Calendar.HOUR_OF_DAY);
    	int minute = cal.get(Calendar.MINUTE);
    	
		m_calAutoStartCalendar.set(Calendar.YEAR, year);
		m_calAutoStartCalendar.set(Calendar.MONTH, month);
		m_calAutoStartCalendar.set(Calendar.DAY_OF_MONTH, day);
		m_calAutoStartCalendar.set(Calendar.HOUR_OF_DAY, hour);
		m_calAutoStartCalendar.set(Calendar.MINUTE, minute);
		m_calAutoStartCalendar.set(Calendar.SECOND, 0);
		m_calAutoStartCalendar.set(Calendar.MILLISECOND, 0);
    	
    	m_btnAutoStartDate.setText(date);
    	m_btnAutoStartTime.setText(time);
    }

    public static class AutoShutDownReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context ctx, Intent arg1) {			
			Log.d(AnsweringMachineService.LOG_TAG, "Scheduler stopping Answering Machine Service!");
			ctx.stopService(new Intent(ctx,AnsweringMachineService.class));
			
			//send new broadcast to update UI
			Intent intent = new Intent();
			intent.setAction(ACTION_ALARM_SHUTDOWN_EVENT);
			ctx.sendBroadcast(intent);
		}
    	
    }
    
    public static class AutoStartReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context ctx, Intent receivedIntent) {
			Log.d(AnsweringMachineService.LOG_TAG, "Scheduler: AutoStartReceiver activated!");
			
			Bundle bundle = new Bundle();
			addSettingsToBundle(bundle, ctx);

			Log.d(AnsweringMachineService.LOG_TAG, "Scheduler: AutoStartReceiver: Everything OK, trying to start AM now!!");

			Intent serviceIntent = new Intent(ctx,AnsweringMachineService.class);
			serviceIntent.putExtras(bundle);
			ctx.startService(serviceIntent);

			//send new broadcast to update UI
			Intent intent = new Intent();
			intent.setAction(ACTION_ALARM_START_EVENT);
			ctx.sendBroadcast(intent);

		}
    	
    }
    
    private void setScheduledAutoShutDown (boolean val, long time) {    	
    	SharedPreferences settings = getSharedPreferences(mainActivity.PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();        
        
        editor.putLong(PREFS_AUTO_SHUT_DOWN_TIME, time);
        editor.putBoolean(PREFS_AUTO_SHUT_DOWN_SCHEDULED, val);       
        
        // Commit the edits!
        editor.commit();
    }
    
    private boolean isAutoShutDownScheduled() {
    	boolean result = false;
    	boolean expired = false;
    	
    	SharedPreferences settings = getSharedPreferences(mainActivity.PREFS_NAME, 0);
    	
    	long time = settings.getLong(PREFS_AUTO_SHUT_DOWN_TIME, -1);
    	long now = -1;
    	
    	if (time > 0) {
    		now = Calendar.getInstance().getTimeInMillis();
    		if (now > time) {
    			expired = true;    			
    		}
    	}
    	
    	android.text.format.DateFormat df = new android.text.format.DateFormat();
    	String strdate = (String) df.format("yyyy-MM-dd", time);
    	String strtime = (String) df.format("hh:mm A", time);
        Log.d(AnsweringMachineService.LOG_TAG, "Shut down was scheduled for: " + strdate + ": " + strtime);
		
        strdate = (String) df.format("yyyy-MM-dd", now);
    	strtime = (String) df.format("hh:mm A", now);
        Log.d(AnsweringMachineService.LOG_TAG, "Current time: " + strdate + ": " + strtime);
    	
    	boolean wasScheduled = settings.getBoolean(PREFS_AUTO_SHUT_DOWN_SCHEDULED, false);
    	
    	if (!expired && wasScheduled) {
    		result = true;
    	}
    	
    	return result;
    }
    
    

    private void setAutoStartScheduled (boolean val, long time) {    	
    	SharedPreferences settings = getSharedPreferences(mainActivity.PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();        
        
        editor.putLong(PREFS_AUTO_START_TIME, time);
        editor.putBoolean(PREFS_AUTO_START_SCHEDULED, val);       
        
        // Commit the edits!
        editor.commit();
    }

    private boolean isAutoStartScheduled() {
    	boolean result = false;
    	boolean expired = false;
    	
    	SharedPreferences settings = getSharedPreferences(mainActivity.PREFS_NAME, 0);
    	
    	long time = settings.getLong(PREFS_AUTO_START_TIME, -1);
    	long now = -1;
    	
    	if (time > 0) {
    		now = Calendar.getInstance().getTimeInMillis();
    		if (now > time) {
    			expired = true;    			
    		}
    	}
    	
    	android.text.format.DateFormat df = new android.text.format.DateFormat();
    	String strdate = (String) df.format("yyyy-MM-dd", time);
    	String strtime = (String) df.format("hh:mm A", time);
        Log.d(AnsweringMachineService.LOG_TAG, "Start was scheduled for: " + strdate + ": " + strtime);
		
        strdate = (String) df.format("yyyy-MM-dd", now);
    	strtime = (String) df.format("hh:mm A", now);
        Log.d(AnsweringMachineService.LOG_TAG, "Current time: " + strdate + ": " + strtime);
    	
    	boolean wasScheduled = settings.getBoolean(PREFS_AUTO_START_SCHEDULED, false);
    	
    	if (!expired && wasScheduled) {
    		result = true;
    	}
    	
    	return result;
    }

        
    private void restoreAutoShutDownToggleButtonState() {
    	boolean val = isAutoShutDownScheduled();
    	m_tglBtnAutoShutDown.setChecked(val);
    }   
    
    private void restoreAutoStartToggleButtonState() {
    	boolean val = isAutoStartScheduled();
    	m_tglBtnAutoStart.setChecked(val);
    }   
    
    private boolean isEMailForwardingOn() {
    	boolean result = false;
    	
    	SharedPreferences settings = getSharedPreferences(mainActivity.PREFS_NAME, 0);
        
    	if (settings != null) {	        
        	boolean boolFMC = settings.getBoolean(PREFS_SETTINGS_FORWARD_TO_EMAIL_MC, false);
        	boolean boolFTM = settings.getBoolean(PREFS_SETTINGS_FORWARD_TO_EMAIL_TEXT_M, false);
        	if (boolFMC || boolFTM) {
        		result = true; // or simply result = boolFMC || boolFTM
        	}
        }
    	
    	return result;
    }

}