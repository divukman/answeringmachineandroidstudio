package com.mitosoft.answeringmachine.alarms;

import com.mitosoft.sms.AnsweringMachineService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AlarmReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context ctx, Intent intent) {	
		//Stop the Answering Machine Service		
		ctx.stopService(new Intent(ctx,AnsweringMachineService.class));
	}
	
}
