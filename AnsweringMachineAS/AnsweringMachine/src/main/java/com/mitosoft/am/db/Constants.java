package com.mitosoft.am.db;

/**
CREATE TABLE [messages] (
	  [ID] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
	  [message] TEXT NOT NULL);

CREATE TABLE [schedules] (
	  [ID] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
	  [MESSAGE_ID] INTEGER NOT NULL CONSTRAINT [MESSAGE_ID] REFERENCES [messages]([ID]), 
	  [start] NUMERIC NOT NULL, 
	  [end] NUMERIC NOT NULL, 
	  [active] BOOLEAN NOT NULL);
**/

public class Constants {
	public static final String DATABASE_NAME = "amdatabase";
	public static final int DATABASE_VERSION = 1;
	
	//id field name for all tables
	public static final String FIELD_ID = "_id";
	
	// table messages
	public static final String TABLE_NAME_MESSAGES = "messages";
	public static final String FIELD_TITLE = "title";
	public static final String FIELD_MESSAGE = "message";
	
	//table schedules
	public static final String TABLE_NAME_SCHEDULES = "schedules";
	public static final String FIELD_MESSAGE_ID = "MESSAGE_ID";
	public static final String FIELD_START = "start";
	public static final String FIELD_END = "end";
	public static final String FIELD_ACTIVE = "active";
}
