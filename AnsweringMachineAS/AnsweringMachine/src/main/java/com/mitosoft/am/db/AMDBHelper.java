package com.mitosoft.am.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class AMDBHelper extends SQLiteOpenHelper {

	private static final String LOG_TAG = "AMDBHelper";
	
	/*CREATE TABLE [messages] (
	  [ID] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
	  [title] VARCHAR(128) NOT NULL, 
	  [message] TEXT NOT NULL);
	 **/
	private static final String CREATE_TABLE_MESSAGES = 
			"CREATE TABLE [" + 
					Constants.TABLE_NAME_MESSAGES +
					"] ( [" + Constants.FIELD_ID + 		"] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," 
					//+ "[" + Constants.FIELD_TITLE + 	"] VARCHAR(128) NOT NULL,"
					+ "[" + Constants.FIELD_MESSAGE + 	"] TEXT NOT NULL);";
	
	/*CREATE TABLE [schedules] (
	  [ID] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
	  [MESSAGE_ID] INTEGER NOT NULL CONSTRAINT [MESSAGE_ID] REFERENCES [messages]([ID]), 
	  [start] NUMERIC NOT NULL, 
	  [end] NUMERIC NOT NULL, 
	  [active] BOOLEAN NOT NULL);
	 **/
//	private static final String CREATE_TABLE_SCHEDULES = 
//				"CREATE TABLE [" +
//				Constants.TABLE_NAME_SCHEDULES +
//				"] ( [" + 
//				Constants.FIELD_ID + "] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," + 
//				"[" + Constants.FIELD_MESSAGE_ID + "] INTEGER NOT NULL CONSTRAINT [" + Constants.FIELD_MESSAGE_ID + "] REFERENCES [" + Constants.TABLE_NAME_MESSAGES + "]([" + Constants.FIELD_ID + "]), " +
//				"[" + Constants.FIELD_START + "] NUMERIC NOT NULL," + 
//				"[" + Constants.FIELD_END + "] NUMERIC NOT NULL," + 
//				"[" + Constants.FIELD_ACTIVE + "] BOOLEAN NOT NULL,"
//					;
	
	public AMDBHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);		
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.v(LOG_TAG, "Creating tables.");
		try {
			db.execSQL(CREATE_TABLE_MESSAGES);
			//db.execSQL(CREATE_TABLE_SCHEDULES);
		} catch (SQLiteException e) {
			Log.v("Create table exception", e.getMessage());
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(LOG_TAG + " db upgrade", "Upgrading from version " + oldVersion + " to " + newVersion);
		db.execSQL("drop table if exists " + Constants.TABLE_NAME_MESSAGES);
		db.execSQL("drop table if exists " + Constants.TABLE_NAME_SCHEDULES);
		onCreate(db);
	}

}
