package com.mitosoft.am.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

public class AMDB {
	
	private SQLiteDatabase m_db = null;
	private final AMDBHelper m_dbHelper;
	
	public AMDB(Context context) {
		m_dbHelper = new AMDBHelper(context, Constants.DATABASE_NAME, null, Constants.DATABASE_VERSION);
	}
	
	
	public void close() {
		m_db.close();
	}
	
	
	public void open() {
		try {
			m_db = m_dbHelper.getWritableDatabase();
		} catch (SQLiteException e) {
			Log.v("Open database exception caught: ", e.getMessage());
			m_db = m_dbHelper.getReadableDatabase();
		}
	}
	
	
	/**
	 * Inserts a message to the messages table.
	 **/
	public long insertMessage(String message) {
		long result = -1;
		
		try {
			ContentValues newMessageValue = new ContentValues();								
			newMessageValue.put(Constants.FIELD_MESSAGE, message);			
			result = m_db.insert(Constants.TABLE_NAME_MESSAGES, null, newMessageValue);			
		} catch(SQLiteException e) {
			Log.v("Insert into database exception caught!", e.getMessage());
		}
		
		return result;
	}
	
	
	/**
	 * Gets the message id.
	 * @param message text
	 * @return row id
	 **/
	public long getMessageRowID(String message) {
		long result = -1;
		
		String p_query = "select * from messages where message = ?";
		Cursor resultSet = m_db.rawQuery(p_query, new String[] {message});
		
		resultSet.moveToFirst();
		if (resultSet.getCount() > 0) {
			int columnIndex = resultSet.getColumnIndex(Constants.FIELD_ID);
			result = resultSet.getLong(columnIndex); 
		}

		return result;
	}
	
	
	/**
	 * Deletes a message from the messages table.
	 * @param message text
	 * @return number of deleted rows, -1 if nothing is deleted or exception
	 **/
	public long deleteMessage(long id) {
		long result = -1;
		
		try {
			result = m_db.delete(Constants.TABLE_NAME_MESSAGES, Constants.FIELD_ID+ "=" + id, null);						
		} catch (SQLiteException e) {
			Log.v("Deleting from the database exeption caught!", e.getMessage());
		}
		
		return result;
	}
	
	/**
	 * Returns all of the messages from the messages table.
	 * @return Cursor containing the messages
	 **/
	public Cursor getMessages() {
		Cursor c = m_db.query(Constants.TABLE_NAME_MESSAGES, null, null, 
								null, null, null, null);
		return c;
	}
	
	
	/**
	 * Checks if message is present in the database.
	 * @param message text
	 * @return boolean, true if present, false otherwise
	 **/
	public boolean isMessageInTheDatabase(String message) {
		boolean result = false;

		String p_query = "select * from messages where message = ?";
		Cursor resultSet = m_db.rawQuery(p_query, new String[] {message});
		
				
		//Cursor resultSet = m_db.rawQuery("SELECT _id " +
		//"FROM " + Constants.TABLE_NAME_MESSAGES + " WHERE " + Constants.FIELD_MESSAGE +"=" + message , null );

		resultSet.moveToFirst();

		result = resultSet.getCount() > 0;
		return result;
	}
}
