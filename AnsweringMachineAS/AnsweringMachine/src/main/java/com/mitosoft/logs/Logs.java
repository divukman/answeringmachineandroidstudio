package com.mitosoft.logs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.mitosoft.sms.AnsweringMachineService;
import com.mitosoft.sms.R;
import com.mitosoft.sms.SettingsActivity;
import com.mitosoft.sms.mainActivity;
import com.mitosoft.sms.R.id;
import com.mitosoft.sms.R.layout;
import com.mitosoft.sms.email.SendEMail;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Logs extends Activity {   
	
	 EditText m_tv = null; 
	 Button m_btnSend = null;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.logs);
      try {
    	  	m_tv = (EditText)findViewById(R.id.editTextLogs);
    	  	m_btnSend = (Button)findViewById(R.id.buttonSend); 
    	  
    	  	m_btnSend.setOnClickListener(new OnClickListener() {
				
				
				public void onClick(View v) {
					String logs = m_tv.getText().toString().trim();
					if (logs.length() > 0) {
						sendEmail3(logs);
					} else {
						Toast.makeText(Logs.this, "No logs.", Toast.LENGTH_LONG).show();
					}
					
				}
			});
    	  	
	        Process process = Runtime.getRuntime().exec("logcat -d");
	        BufferedReader bufferedReader = new BufferedReader(
	        new InputStreamReader(process.getInputStream()));
	                         
	        StringBuilder log=new StringBuilder();
	        String line;
	        int counter = 1;
	        while ((line = bufferedReader.readLine()) != null) {
	        	//add some logic to filter out only app logs
	        	if (line.contains(AnsweringMachineService.LOG_TAG)) {
	        		line = (counter++) + " : " + line + "\n";
	        		log.append(line);
	        		m_tv.append(line);
	        	}
	        }
      } catch (IOException e) {
    	  Log.e(AnsweringMachineService.LOG_TAG, "Logs reading exception :" + e.getMessage());
      }
    }
	
	private void sendEmail3(String text) {
		// Setup the recipient in a String array
		String[] mailto = { "divukman@gmail.com" };		
		// Create a new Intent to send messages
		Intent sendIntent = new Intent(Intent.ACTION_SEND);
		// Add attributes to the intent
		sendIntent.putExtra(Intent.EXTRA_EMAIL, mailto);		
		sendIntent.putExtra(Intent.EXTRA_TEXT, text);
		sendIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
        "Android SMS Answering Machine - Logs");
		//sendIntent.setType("message/rfc822");
		sendIntent.setType("text/plain");
		startActivity(Intent.createChooser(sendIntent, "Please pick your preferred email application"));
	}
	
  }

