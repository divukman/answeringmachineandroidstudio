package com.mitosoft.event;


public class EventType {
	
	public final String name;
	
	protected EventType (String name) {
		this.name = name;
	}

	public static final EventType SMS_RECEIVED = new EventType("SMS Received Event Type");
	public static final EventType CALL_MISSED = new EventType("Call Missed Event Type");
}
