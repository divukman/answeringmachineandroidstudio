package com.mitosoft.event;

public class CommunicationEvent extends Event {

	private final String m_address; 
	private final String m_message;
	
	public CommunicationEvent(Object source, EventType type, String address, String message) {
		super(source, type);		
		m_address = address;
		m_message = message;
	}

	public String getAddress() {
		return m_address;
	}
	
	public String getMessage() {
		return m_message;
	}
}
