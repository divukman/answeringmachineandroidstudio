package com.mitosoft.event;

public interface ICommEventListener {
	public void onEvent(CommunicationEvent e);
}
