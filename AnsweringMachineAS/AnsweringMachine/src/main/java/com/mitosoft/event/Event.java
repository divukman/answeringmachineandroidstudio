package com.mitosoft.event;

import java.util.EventObject;

public class Event extends EventObject {

	private static final long serialVersionUID = 1L;
	private EventType m_type = null;

	public Event(Object source, EventType type) {
		super(source);
		m_type = type;
	}
	
	public EventType getType() {
		return m_type;
	}

}
